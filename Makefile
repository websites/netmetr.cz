PY?=python3
PELICAN?=pelican

BASEDIR=$(CURDIR)
OUTPUTDIR=$(BASEDIR)/`grep OUTPUT_PATH pelicanconf.py| sed 's/.*=\ //;s/"//g'`

all:	build

help:
		@echo 'Makefile for NetMetr.cz website.                                 '
		@echo '                                                                 '
		@echo 'Usage:                                                           '
		@echo ' make clean              remove the generated files & cache      '
		@echo ' make build              build the website (default target)      '
		@echo ' make publish-production build and upload to netmetr.cz          '
		@echo ' make publish-prototype  build and upload to devpub/netmetr-proto'
		@echo '                                                                 '
		@echo ' (set $DEVPUB_USER to your remote username for publish targets   '
		@echo '  eg. DEVPUB_USER=foobar make publish-prototype)                 '

clean:
		[ ! -d $(OUTPUTDIR) ] || rm -rf $(OUTPUTDIR)
		[ ! -d cache ] || rm -rf cache
		[ ! -d __pycache__ ] || rm -rf __pycache__

build:	clean
		./build.sh

publish-production:	build
		rsync -ru  --exclude '*-cache*' --exclude '*.tmp' --exclude 'tmp*/*' --delete $(OUTPUTDIR)/* $(DEVPUB_USER)@devpub.labs.nic.cz:/var/www/openrmbt/

publish-prototype: build
		rsync -ru  --exclude '*-cache*' --exclude '*.tmp' --exclude 'tmp*/*' --delete $(OUTPUTDIR)/* $(DEVPUB_USER)@devpub.labs.nic.cz:/var/www/netmetr-proto/

.PHONY:		clean
