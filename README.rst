======================================
Pelican sources for netmetr.cz website
======================================

Preparation
===========

Creating virtualenv
-------------------

This is not mandatory, but it's handy to create separate environments for such projects, so you don't have to install (possibly conflicting) dependencies system-wide.

There are good tutorials for virtualenv `available online <http://docs.python-guide.org/en/latest/dev/virtualenvs/>`_, but essentially it's just this::

    >> cd netmetr.cz
    >> python -m venv .venv
    >> source .venv/bin/activate

Installing dependencies
-----------------------

**This is what you need to do only after fetching the source for the first
time or after updates.**

After cloning the repository, you have to do the following do make it work::

    >> git submodule init
    >> git submodule update

This will fetch the ``pelican-plugins`` and ``product-template`` repos as git submodules.

Then you need to install requirements. You can do it by running::

    >> pip install -r requirements.txt

inside a virtualenv specifically created for this application.

If you encounter something like ``fatal error: Python.h: No such file or directory``
during ``pip install``, you probably need (on Debian/Ubuntu) to install ``python-dev`` deb package.

Usage
=====

Backend URLs
------------

Set your backend server location in ``theme/static/js/urls.js``. There are three hostnames, but all point
to the same machine, the difference is in their DNS records:

- ``baseUrl`` – has both A and AAAA records, used by most of the scripts
- ``baseUrl4`` – only A record, used for forced IPv4-only speed test
- ``baseUrl6`` – only AAAA record, used for forced IPv6-only speed test

Building
--------

You can build the website by running::

    >> make

in the main directory.

Default ``make`` target uses URLs set in ``pelicanconf.py``.

The output is in the ``output`` directory, but it is
better to view it by running a HTTP server (because of browser policies for scripts)::

    >> cd output
    >> python -m SimpleHTTPServer        # for python 2
    >> python -m http.server             # for python 3

You can specify another port if 8000 is used by something else::

    >> python -m http.server 1234

Deploying
=========

Website is hosted at Gitlab Pages, and is automatically updated after each push to the ``master`` branch.
