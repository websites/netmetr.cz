#!/usr/bin/env bash
set -e;
OUTPUT_PATH=$(grep OUTPUT_PATH pelicanconf.py| sed 's/.*=\ //;s/"//g')
DEFAULT_LANG=$(python3 -c "from pelicanconf import DEFAULT_LANG; print(DEFAULT_LANG)")
SITEURL=$(python3 -c "from pelicanconf import SITEURL; print(SITEURL.replace('/', '\\/'))")

mv content/pages/cs/help.md content/pages/cs/help.tmp
mv content/pages/en/help.md content/pages/en/help.tmp

mv content/pages/cs/terms.md content/pages/cs/terms.tmp
mv content/pages/en/terms.md content/pages/en/terms.tmp

cat content/pages/cs/help.tmp content/pages/cs/help/*.md >> content/pages/cs/help.md
cat content/pages/en/help.tmp content/pages/en/help/*.md >> content/pages/en/help.md

cat content/pages/cs/terms.tmp content/pages/cs/terms/*.md >> content/pages/cs/terms.md
cat content/pages/en/terms.tmp content/pages/en/terms/*.md >> content/pages/en/terms.md

mkdir -p "${OUTPUT_PATH}"

pelican -q
cp theme/static/images/favicons/favicon.ico "${OUTPUT_PATH}"
cp -r content/qostest "${OUTPUT_PATH}"

mv content/pages/cs/help.tmp content/pages/cs/help.md
mv content/pages/en/help.tmp content/pages/en/help.md

mv content/pages/cs/terms.tmp content/pages/cs/terms.md
mv content/pages/en/terms.tmp content/pages/en/terms.md

find output -type f -name \*.css -exec sed -i "s/__SITEURL__/${SITEURL}/g" {} +
find output -type f -name \*.css -exec ./node_modules/.bin/autoprefixer-cli -b "> 5%, last 2 versions, ie 11" {} +

echo "<meta http-equiv='refresh' content='0; url=${DEFAULT_LANG}/' />" > output/index.html
