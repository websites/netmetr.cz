lang: cs
title: Open Data
slug: open-data
template: data
sortorder: 98
older_data_heading: Stažení dat
older_data_button: Stáhnout
slug_en: open-data

Open Data - export výsledků měření
----------------------------------

Výsledky měření jsou dostupné všem zájemcům jako Open Data v licenci [Creative Commons Uveďte původ 4.0 (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.cs).

Specifikace formátu dat je dostupná [zde](open-data-specifikace.html).

Zveřejnění dat je v souladu se zásadami ochrany osobních údajů.

<small>Upozornění: časová razítka jsou v UTC (ne v místním čase).</small>

[![★★★](/theme/images/data-badge-3.png)](http://5stardata.info/cs/)
