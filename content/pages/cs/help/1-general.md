## 1. Obecné otázky

### 1.1 Co to je NetMetr?

NetMetr slouží k důkladnému testování vašeho připojení k internetu přes mobilní sítě a Wi-Fi.

Netmetr nabízí:

- Podrobné testování mobilního připojení
- Zobrazení výsledků testů v mapě s možností jejich filtrování podle různých parametrů
- Statistiky výsledků měření v sítích všech operátorů
- Možnost synchronizovat výsledky testů z různých zařízení a jejich společné zobrazení
- Historii vašich testů
- Získaná data z měření a zdrojové kódy aplikace jsou zveřejněny jako OpenData a OpenSource

### 1.2 Kdo je autorem NetMetru?

NetMetr je založen na zdrojových kódech nástroje RTR-NetTest ([netztest.at](https://www.netztest.at/)) od rakouského telekomunikačního úřadu [Rundfunk und Telekom Regulierungs-GmbH](https://www.rtr.at/).

Samotný NetMetr je vyvíjen sdružením [CZ.NIC](https://www.nic.cz/), zdrojové kódy jsou k dispozici [zde](https://gitlab.nic.cz/labs/netmetr).

### 1.3 Proč si mám testovat připojení?

Šířka pásma je významným parametrem kvality vašeho mobilního připojení k internetu. NetMetr vám nabízí možnost zkontrolovat, jaká je aktuální šířka pásma ve skutečnosti. Další možností je porovnání výsledky testu s hodnotami ostatních uživatelů. Na systému Android podporuje analýzu sítě, nabízí podrobnější test nástroji pro diagnostiku sítě (NDT). Výsledky testů všech operátorů prováděné na nejrůznějších místech mohou být zajímavé pro velké množství uživatelů.

### 1.4 Co se testuje?

Pomocí NetMetru můžete ve svém mobilním zařízení otestovat:

- rychlost stahování (download)
- rychlost nahrávání (upload),
- latence připojení (ping),
- sílu signálu, pokud to zařízení umožňuje

Rozšiřujícím testem (NDT), který je k dispozici na zařízeních se systémem Android, můžete také otestovat další kvalitativní parametry, jako např. ztrátovost paketů.

### 1.5 Jak nás můžete kontaktovat?

Na technickou podporu se můžete obrátit především s dotazy, na které jste zde nenalezli odpověď. Své dotazy zasílejte na <mailto:netmetr@labs.nic.cz>.

### 1.6 Jaké typy připojení je možné testovat?

NetMetr je k dispozici pro mobilní zařízení se systémy Android a iOS (Apple). Můžete testovat jak připojení přes mobilní síť (EDGE, UMTS, LTE), tak přes Wi-Fi.
Na počítačích nebo mobilních zařízeních s nepodporovaným systémem je možné použít webovou verzi testu, funkční ve všech moderních webových prohlížečích.

### 1.7 Jak kontaktovat technickou podporu, pokud mám nějaký problém s NetMetrem?

Před kontaktováním technické podpory nejprve zkuste nalézt odpověď na vaši otázku v tomto FAQ. Pokud odpověď nenaleznete, budeme rádi, když nám napíšete na <mailto:netmetr@labs.nic.cz>.

### 1.8 Jaký je rozdíl mezi NetMetrem a jinými aplikacemi pro měření rychlosti připojení?

NetMetr nabízí:

- nezávislé a nekomerční řešení
- měřící server připojený přes rychlou a kapacitní linku do NIXu
- detailní analýzu připojení pomocí NDT testu
- open source aplikace
– získaná data poskytovaná veřejnosti k další analýze jako OpenData
