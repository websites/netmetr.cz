## 2. Technologie

### 2.1 Jak NetMetr funguje?

NetMetr měří kvalitativní parametry vašeho bezdrátového připojení k Internetu testováním přenosů z vašeho mobilního zařízení na server a obráceně. Z naměřených hodnot se počítají aktuální hodnoty parametrů připojení. Kompletní popis testovací metodologie je k dispozici [zde](https://www.netztest.at/doc/) (v angličtině).

NetMetr může provádět dva různé testy:

- **Základní test** je rychlá analýza rychlostí stahování, nahrávání, latence (pingu) a – u Android verze – síly signálu.
- **Detailní test** (NDT test) je rozšiřující nezávislý test sdružení [M-Lab](<http://www.measurementlab.net>), využívající specifické možnosti diagnostiky spojení v systému Android.
- V systému Android jsou navíc také testovány vlastnosti tzv. *Quality of Service* (nepozměňování obsahu, referenční stránka, transparentní spojení, DNS, TCP a UDP porty).

### 2.2 Co dělá doplňkový test NDT?

Po provedení rychlého základního testu vašeho připojení je možné spustit doplňkový test pro komplexní síťovou analýzu. Používá se při ní Network Diagnostic Tool (NDT) od sdružení M-Lab (http://www.measurementlab.net).

Systém iOS (iPhone, iPad) provádění tohoto testu bohužel neumožňuje.

### 2.3 Jaké výhody přináší doplňkový test NDT?

Podrobnější test prověřuje množství parametrů sítě, které jsou nezbytné pro komplexní analýzu vašeho připojení a nejsou testovány základním testem. Seznam všech síťových parametrů a podrobnosti o NDT naleznete na stránkách [M-Lab](https://code.google.com/p/ndt/w/list).

### 2.4 Jak se moje internetové připojení testuje?

NetMetr měří připojení z vašeho zařízení na testovací server. Ten je připojen rychlou a kapacitní linkou přímo do [internetového uzlu NIX](https://cs.wikipedia.org/wiki/NIX), čímž minimalizuje vliv rychlosti transportu dat na servery vašeho mobilního operátora.

### 2.5 Čemu je třeba věnovat pozornost před zahájením testu?

Pro získání co nejméně zkreslených výsledků testů je třeba splnit několik podmínek:

- V zařízení neběží na pozadí žádné datově náročné aplikace,
- Zařízení je co nejblíže k Wi-Fi AP, aby nedošlo k ovlivnění měření slabým signálem (pokud měříte připojení přes Wi-Fi),
- Nepřekročili jste svůj limit FUP a nebyla tak ze strany operátora snížena rychlost datového připojení (v případě měření mobilního datového připojení)

### 2.6 Je k dispozici zdrojový kód?

NetMetr je open source, zdrojový kód serverové části je k dispozici na <https://gitlab.nic.cz/labs/netmetr> a mobilní aplikace na <https://gitlab.nic.cz/mobile/netmetr_flutter>.

### 2.7 Jakou rychlost připojení potřebuji?

Pro některé aplikace není velká šířka pásma nutná. Pro běžné surfování jsou obvykle naprosto dostačující 2 Mb/s. Některé aplikace však bez dostatečné šířky pásma správně nefungují. Například online přehrávání videa a hudby lze bez výpadků provozovat pouze v případě, že je připojení dostatečně rychlé.

Důležité je vzít v úvahu nejen šířku pásma směrem k uživateli (tedy stahování, download), ale i směrem do Internetu (nahrávání, upload).

### 2.8 Jaké faktory mají vliv na výsledek testu?

- Maximální šířka pásma mobilního připojení k Internetu (omezení rychlostí v závislosti na zvoleném tarifu)
- Využití šířky pásma vašeho poskytovatele internetového připojení
- Zařízení, na kterém se test provádí
- Operační systém zařízení a jeho konfigurace

Při využití Wi-Fi:

- vzdálenost mezi Wi-Fi routerem a vaším zařízením
- kvalita Wi-Fi routeru a technologie, které podporuje (např. 802.11n nebo 802.11ac)

Při využití mobilního připojení:

- Použitá technologie (EDGE, UMTS, LTE),
- Kvalita služeb na místě měření (síla signálu, v závislosti na zařízení),
- Zda jste uvnitř nebo vně budovy,
- Jak rychle se pohybujete (v jedoucím autě na dálnici nebo ve vlaku budete mít nižší přenosovou rychlost),
- Počet uživatelů současně sdílejících základnovou stanici (BTS)

### 2.9 Jaká je optimální denní doba pro provedení testu?

Výsledek testu je mimo jiné ovlivněn denní dobou. Doporučuje se proto provádět test v pravidelných intervalech a v různých denních dobách. Využití Internetu v závislosti na denní době lze odhadnout na základě využití [připojení k hlavnímu českému internetovému uzlu NIX](https://www.nix.cz/ports/aggr/day).

### 2.10 Jaký vliv má na měření můj Wi-Fi router nebo access point?

Wi-Fi router nebo AP může mít na rychlost připojení obrovský vliv.

Pokud váš router nedokáže využít plnou kapacitu vašeho připojení k internetu, ovlivní to výsledky testu. Pro rychlá připojení se vždy doporučuje používat směrovače podporující standardy IEEE 802.11n nebo 802.11ac, samozřejmě pokud je umí připojená zařízení využít.

Je také důležité, aby router zvládal překlad adres (NAT), pokud ho provádí, s vysokou propustností  aby vzdálenost mezi routerem a vaším přístrojem nebyla příliš velká.

V případě, že výsledek testu se zásadně liší od očekávaných hodnot, doporučujeme kontrolu bezdrátového routeru.

### 2.11 Kolik testů je třeba provést, abychom dostali opravdu spolehlivé hodnoty?

Výsledek testu závisí na několika faktorech: denní době, využití sítě a vaší poloze. Můžete zvýšit výpovědní hodnotu vašich výsledků testů jejich opakovaným spuštěním v různých denních dobách a různých místech. Čím vícekrát testy spustíte, tím získáte lepší podklady pro objektivní hodnocení vašeho připojení.

### 2.12 Jak často mohu opakovat testování s NetMetrem?

Testování NetMetrem můžete opakovat tak často, jak chcete. Test v mobilních aplikacích můžete například opakovat hned po jeho skončení, stačí kliknout na tlačítko „Nový test“.

Před opakováním je nutné, aby byl dokončen podrobnější test (NDT, platí pouze pro Android).

**Při testu se přenášejí velké objemy dat** - pokud testujete připojení přes mobilní síť, tak to může znamenat rychlé překročení vašeho limitu FUP a následné snížení rychlosti ze strany operátora.

Pravidelné testy zvýší výpovědní hodnotu dosažených výsledků.

### 2.13 Jaké objemy dat se v testu přenášejí?

Přenesený objem dat se liší v závislosti na šířce pásma připojení k Internetu. NetMetr testuje stahování a odesílání kapacitu vašeho připojení k internetu po dobu asi 20 sekund nejvyšší dostupnou rychlostí. V závislosti na dostupné šířce pásma připojení to může mít za následek zvýšení využití dat.

- Při měření přes GPRS test přenese cca. 130 kB dat, test přes HSDPA je podstatně datově náročnější, spotřebuje přes 14 MB dat.
- S velmi rychlým připojením k internetu NetMetr použije velmi velké objemy dat, například se symetrickým 100 Mb/s připojením je objem dat cca. 250 MB.
- Pokud se provádí také podrobnější testování pomocí NDT testu, objem dat se zvýší přibližně 2,5×.

Vezměte prosím na vědomí, že přenos dat může způsobit u připojení přes mobilní síť a v závislosti na vašem tarifu omezení rychlosti ze strany operátora (při překročení FUP).

### 2.14 Jaká se používá verze IP protokolu?

Ve výchozím nastavení používá aplikace k testování IPv6, pokud je k dispozici. V opačném případě se použije IPv4. V aplikaci můžete protokol IPv6 zakázat (v „Nastavení“).

Více informací o IPv6 najdete na [stránce IPv6 na Wikipedii](https://cs.wikipedia.org/wiki/IPv6).
