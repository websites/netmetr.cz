## 3. Výsledky testů

### 3.1 Co znamená download?

Download znamená stahování, přenos dat z internetu do vašeho zařízení. Jednotka „Mb/s“ znamená počet megabitů za sekundu.

Rychlost přenosu dat, kterou inzerují např. internetoví poskytovatelé, obvykle znamená *absolutní* rychlost přenosu dat, tedy současný přenos i dalších dat přenášených v důsledku režie používaného síťového protokolu. NetMetr ovšem měří *reálné* rychlosti připojení. Tyto hodnoty jsou průměrně asi o 10 % nižší, než je absolutní přenosová rychlost vašeho připojení k internetu.

### 3.2 Co znamená upload?

Upload znamená nahrávání, přenos dat z vašeho zařízení do internetu. Rychlost odesílání dat se obyčejně v marketingových materiálech neobjevuje a je obvykle mnohem nižší, než je rychlost stahování dat. Ale pro rychlý přístup k internetu má význam podobný, jako rychlost stahování dat, protože komunikace na internetu probíhá prakticky vždy obousměrně. Rychlost nahrávání dat je zvláště důležitá, pokud chcete odesílat fotografie, využívat technologie pro sdílení souborů nebo např. videohovory.

Stejně jako tomu je u rychlosti stahování je i zde rozdíl mezi „absolutní“ a „reálnou“ rychlostí přenosu dat.

### 3.3 Co znamená latence (ping)?

Latence (slangově i v češtině ping) označuje rychlost odezvy serveru – tedy čas potřebný k přenosu malého množství dat z vašeho přístroje na server a zpět. Měří se v milisekundách (ms). Latence je velmi důležitá nejen pro hraní on-line her, i u zcela běžného surfování má vysoká latence značný vliv na jeho komfort. Toto zpoždění zásadně ovlivňuje jak technologie vašeho připojení k internetu, tak také její využití (např. kolik uživatelů je aktuálně současně připojeno).

Rychlá připojení k internetu mají latenci obvykle menší, než 2 ms (rychlé metalické sítě a optická vlákna), pomalá připojení mohou mít časy i více než 500 ms (GSM); typické hodnoty jsou v rozmezí od 10 do 50 ms. Samozřejmě záleží i na tom, jak je připojen server. Pokud se připojujete na velmi vzdálené servery, má délka přenosové trasy také výrazný vliv na latenci (např. pro Japonsko to může být z Evropy zhruba 280 ms i na rychlých připojeních.).

### 3.4 Co znamená síla signálu?

Čím silnější je signál, který vaše zařízení přijímá od základnové stanice (nebo přístupového bodu Wi-Fi), tím lze očekávat rychlejší a stabilnější připojení k internetu. Pokud je váš mobilní telefon venku nebo např. v blízkosti otevřeného okna, je signál obvykle lepší než uvnitř budov. Pro připojení pomocí sítě Wi-Fi je důležité, aby signálu stínilo co nejméně překážek (např. stěn) mezi Wi-Fi přístupovým bodem a zařízením (např. tabletem).

### 3.5 Jak rozumět ukazateli síly signálu?

Síla signálu (v dBm) se udává v záporných číslech. Čím je naměřená hodnota vyšší (tzn. bližší k nule), tím je signál silnější. Například hodnota -50 dBM znamená velmi dobrý signál, hodnota -113 dBm značí v GSM/UMTS signál velmi slabý.

### 3.6 Jak poznám, jestli je výsledek testu dobrý nebo špatný?

Barva ikonky (červená/žlutá/zelená) u naměřených hodnot rychlostí přenosu dat signalizuje:

- <span class="classification inline" data-classification="3"></span> **Zelená:** hodnota je 10&nbsp;Mb/s nebo vyšší (download), resp. 2&nbsp;Mb/s nebo vyšší (upload)
- <span class="classification inline" data-classification="2"></span> **Žlutá:** hodnota je mezi 2 a 10 Mb/s (download), resp. mezi 1 a 2&nbsp;Mb/s (upload)
- <span class="classification inline" data-classification="1"></span> **Červená:** hodnota je pod 2&nbsp;Mb/s (download), resp. pod 1&nbsp;Mb/s (upload)

Červená/žlutá/zelená škála se zobrazuje bez ohledu na připojení a odpovídá výhradně naměřeným rychlostem. Velmi vysokých rychlostí lze dosáhnout pouze prostřednictvím některých technologiích, jako je HSPA+ nebo LTE. Proto červená ikonka u výsledku nemusí nutně znamenat špatný výsledek, pokud v oblasti kde testujete, je k dispozici pouze 2G síť.

Barva ikonky u hodnost odezvy (ping) signalizuje:

- <span class="classification inline" data-classification="3"></span> **Zelená:** hodnota je 25&nbsp;ms nebo nižší
- <span class="classification inline" data-classification="2"></span> **Žlutá:** hodnota je mezi 25 a 75&nbsp;ms
- <span class="classification inline" data-classification="1"></span> **Červená:** hodnota je 75&nbsp;ms nebo vyšší

### 3.7 Jak správně interpretovat výsledky testu?

Výsledek měření zachycuje momentální stav kvality vašeho přístupu k internetu. **Pokud jsou výsledky testů nižší než hodnoty inzerované operátorem, nemusí to nutně znamenat, že neplní smluvní podmínky.**

Síťové spojení může být ovlivněno různými technologickými faktory, jako je počet dalších uživatelů, kteří jsou současně připojeni na stejné základnové stanici mobilní sítě, kvalita Wi-Fi routeru, konfigurace systému, atd.

V každém případě opravdu spolehlivých výsledků dosáhnete pouze pokud budete měření častěji opakovat a současně uvážíte možné systémové i náhodné chyby měření.


### 3.8 Co znamená jednotka Mb/s?

Rychlost datového připojení se obvykle inzeruje v Mb/s (např. 8 Mb/s), což je zkratka pro megabit za sekundu. Používají se následující přepočty:

- 1 Mb/s = 1000 kb/s
- 1 kb/s = 1000 b/s

Kromě používání jednotky b/s je zvykem používat také byte/s (B/s, např. informace o rychlosti stahování v internetových prohlížečích):
Protože 1 byte/s = 8 b/s, rychlost připojení k internetu lze popsat jako 8 Mb/s, 8000 kb/s nebo 1 MB/s.

Stahování 3 MB souboru na 8 Mb/s připojení bude trvat zhruba 3 sekundy.

### 3.9 Existuje někde žebříček operátorů podle rychlosti?

Účelem aplikace NetMetr není vytvářet žebříčky operátorů, ale spíše poskytnout uživateli komplexní informace o kvalitě jejich připojení k internetu. Statistiky ze všech výsledků testů, rozdělené podle operátorů, lze najít [na stránce Statistiky](statistiky.html) nebo rozdělené po jednotlivých měsících [na stránce Měsíční výsledky](mesicni-vysledky.html)

Při posuzování nabídek operátorů mějte prosím vždy na paměti, že uváděné hodnoty jsou obvykle ty maximální teoreticky možné a že reálná šířka pásma je velmi často mnohem nižší.

### 3.10 Můžu spustit test, i když ještě neskončil NDT test?

Ne, dva testy není možné provádět současně.

### 3.11 Může být špatný výsledek testu způsoben překročením měsíčního limitu objemu přenesených dat (FUP)?

Ano, to je velmi častý problém. FUP, tedy limit objemu přenesených dat, se uplatňuje u naprosté většiny mobilních tarifů. Po dosažení určité hodnoty přenesených dat (oběma směry) operátor sníží rychlost připojení. Tento limit se liší v závislosti na operátorovi a vašem tarifu.
V případě, že toto omezení váš operátor uplatňuje, bude to mít přirozeně negativní vliv na naměřené hodnoty rychlostí.

### 3.12 Co to je synchronizační kód?

Výsledky testů z různých zařízení je možné synchronizovat, abyste měli v každé z aplikací přehled i o svých ostatních připojených zařízeních. Za tímto účelem musí uživatel získat synchronizační kód pomocí aplikace NetMetr a zadat jej v příslušných zařízeních.

Synchronizační kód lze zadat (a výsledky zobrazovat) také na stránce [Moje měření](moje.html) na webu NetMetru.

### 3.13 Mám problémy s měřením síly signálu

Některá zařízení mohou zobrazovat nesprávnou nebo neaktuální sílu signálu mobilní sítě (mimo jiné to bylo pozorováno u některých telefonů Samsung Galaxy S2). V takových případech se měření síly signálu v aplikaci nezobrazuje.

### 3.14 Co znamená QoS?

QoS je zkratka z Quality of Service. Jde o soubor pravidel, jak pro určitá data nastavit maximální/minimální přenosové rychlosti, některý provoz preferovat a podobně. Více na [Wikipedii](https://cs.wikipedia.org/wiki/Quality_of_Service).
