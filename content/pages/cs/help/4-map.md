## 4. Mapa

### 4.1 Jaké výsledky se na mapě zobrazují?

Na mapě se zobrazují výsledky měření standardního testu, naproti tomu výsledky volitelného podrobnějšího testu NDT na mapě nejsou, jsou k dispozici pouze jako tabulková data. Mapa, jak v mobilní aplikaci nebo v prohlížeči, nabízí možnost si pomocí filtrů zvolit, jaká konkrétní data budou na mapě zobrazena.

### 4.2 Zobrazují se i výsledky měření provedené mimo Českou republiku?

Ano, zobrazují se výsledky z celého světa. Měření není nijak geograficky omezeno.

### 4.3 Co to je percentil a medián?

Percentil je statistický ukazatel popisující hodnotu, pod kterou leží X % všech naměřených hodnot. Například, 20% percentil je hodnota, pod kterou spadá 20 % všech výsledků testů.
Medián je 50% percentil, tedy je to hodnota, rozdělující všechny naměřené hodnoty na dvě stejně velké množiny. Jinými slovy, je stejný počet měření s hodnotou menší a stejně tak s hodnotou větší než je medián.

### 4.4 Jak přesně umístěné jsou body s naměřenými hodnotami?

Lokalizace měření se provádí v průběhu měření v závislosti na možnostech a nastavení vašeho přístroje a to pomocí

- GPS,
- informací ze sítě (Wi-Fi nebo mobilní)
- nebo pomocí IP adresy

Lokalizace pomocí GPS je nejpřesnější, zatímco prostřednictvím mobilní sítě získáme pouze přibližnou informaci o poloze. Určení polohy pomocí IP adresy je velmi často značně nepřesné.

Měření s přesností menší než 2 km se na mapě nezobrazují. Měření s přesností menší než 10 km se neuvádějí v detailech. Proto doporučujeme před měřením aktivovat GPS (pokud je k dispozici).

Údaje o přesnosti konkrétních měření jsou k dispozici jako součást [open dat](open-data.html).
