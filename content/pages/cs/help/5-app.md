## 5. Aplikace pro mobilní zařízení

### 5.1 Jaká oprávnění aplikace NetMetr vyžaduje?

Při instalaci aplikace vyžaduje následující oprávnění:

- Poloha - přibližná poloha (pomocí sítě) nebo přesná poloha (pomocí GPS a sítě), umožňuje například lokalizovat výsledek testu na mapě
- Fotky/média/soubory - toto oprávnění je nutné pro ukládání map do paměti
- Řízení hardwaru - zabránit zařízení v přechodu do režimu spánku - měření by mohlo být zkreslené, pokud zařízení během testu přejde do režimu spánku
- Informace o připojení Wi-Fi

### 5.2 Jaké operační systémy jsou podporovány?

Android a Apple iOS. Na nepodporovaných operačních systémech můžete použít [webový test](test.html).

### 5.3 Jaké verze Androidu jsou podporovány?

Verze Android 4.0.3 a vyšší.

### 5.4 Jaké verze iOS jsou podporovány?

Verze iOS 7 a vyšší.

### 5.5 Co je důležité vědět před použitím aplikace?

- Pokud máte tarif s limitem objemu datových přenosů (tzv. FUP), používejte NetMetr pro měření mobilního připojení uvážlivě. Při jednom testu se přenáší poměrně vysoké množství dat (skutečný objem záleží především na typu použitého spojení). Tento problém se měření při připojení přes WiFi síť netýká.
- Určité faktory (jako je denní doba, poloha uvnitř/vně budov, síla signálu, atd.), mají na výsledek testu v mobilních sítích velký vliv
- Po překročení svého limitu měsíčního objemu dat vám mobilní operátor sníží rychlost připojení. To samozřejmě ovlivní výsledky testů.

### 5.6 Je možné používat NetMetr na tabletech?

NetMetr na tabletech funguje a je na ně optimalizován.

### 5.7 Jak nainstalovat Android aplikaci?

Aplikaci NetMetr pro Android nainstalujete na [Google Play](https://play.google.com/store/apps/details?id=cz.nic.netmetr).

### 5.8 Jak nainstalovat iOS aplikaci?

Aplikaci NetMetr pro iOS nainstalujete na [Apple AppStore](https://itunes.apple.com/cz/app/netmetr/id946478662).
