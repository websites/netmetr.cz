## 6. Webový test

### 6.1 Jaké verze prohlížečů jsou podporovány?

Webový test NetMetru funguje na všech verzích majoritních prohlížečů podporovaných jejich výrobci. K provedení testu je potřeba funkční [podpora technologie Web Sockets](http://caniuse.com/#feat=websockets). Přehled o výrobci podporovaných verzích přohlížečů najdete například na Wikipedii:

- [Google Chrome](https://en.wikipedia.org/wiki/Google_Chrome_release_history),
- [Mozilla Firefox](https://en.wikipedia.org/wiki/Firefox_release_history#Release_history),
- [MS Internet Explorer](https://en.wikipedia.org/wiki/Internet_Explorer_versions#Release_history_for_desktop_Windows_OS_version).

### 6.2 Co všechno webový test měří?

Pouze [download](napoveda.html#31-co-znamena-download), [upload](napoveda.html#32-co-znamena-upload) a [ping](napoveda.html#31-co-znamena-latence-ping).

Pokočilé měření QoS nebo parametrů sítě NDT testem umožňuje pouze [aplikace pro Android](aplikace.html).

### 6.3 Je potřeba instalovat do prohlížeče nějaké pluginy?

Není. Stačí moderní prohlížeč s [podporou Web Sockets](http://caniuse.com/#feat=websockets).

### 6.4 Proč se na začátku testu prohlížeč ptá na moji polohu?

Výsledky testů se při zjištění polohy s dostatečnou přesností zobrazují v [mapě](mapa.html). Pokud svoji polohu zveřejnit nechcete, nemusíte – na výsledek testu to nemá vliv.

Určení polohy bez GPS nemusí být příliš přesné, výsledky s velkou nejistotou se pak na mapě nezobrazují, podrobnosti zde – [4.4 Jak přesně umístěné jsou body s naměřenými hodnotami](napoveda.html#44-jak-presne-umistene-jsou-body-s-namerenymi-hodnotami).


### 6.5 Je možné zobrazit společně výsledky mých webových i mobilních testů?

Ano, je. V aplikaci na mobilním zařízení si vygenerujte synchronizační kód a zadejte ho na stránce [Moje měření](moje.html). V tabulce se pak zobrazí i testy z mobilní aplikace.
