lang: cs
title: Úvod
slug: index
template: index
sortorder: 0
icon_app: Aplikace pro<br>mobilní&nbsp;zařízení<p>Stáhněte si testovací aplikaci pro Android nebo iOS.</p>
icon_map: Mapa<p>Zobrazení výsledků měření na mapě.</p>
icon_stats: Statistiky<p>Přehledy zpracovaných výsledky měření.</p>
terms_link_text: Podmínky použití a ochana osobních údajů
terms_link_url: podminky.html

Netmetr je nástroj pro měření aktuální kvality služeb přístupu k Internetu, jako je rychlost stahování a nahrávání, doba odezvy, a u rádiových sítí také síla signálu. Umožňuje uživatelům provádět důkladné a podrobné testování a získat tak komplexní informace nejenom o stavu jejich konkrétního připojení, ale analyzovat i minulá měření a především měření dalších účastníků. Všechna data (přirozeně kromě osobních údajů) a zdrojové kódy jsou uvolňovány na principu Open Source.
