lang: cs
title: Moje měření
slug: moje
template: my-measurements
slug_en: my
sortorder: 5
table_heading: Historie měření
sync_heading: Spojení výsledků z více zařízení

Výsledky testů z různých zařízení je možné synchronizovat a zobrazovat společně. Spárování provedete zadáním synchronizačního kódu, který si vygenerujete v aplikaci na příslušném zařízení.
