lang: cs
title: Open data – specifikace formátu
slug: open-data-specifikace
slug_en: open-data-specs
sortorder: -1

<table class="opendata table">
    <thead>
        <tr>
            <th>Název</th>
            <th>Popis</th>
            <th>Typ</th>
            <th>Příklad</th>
        </tr>
    </thead>
    <tr>
        <td>open_uuid</td>
        <td>Identifikace klienta, který spustil test</td>
        <td>String</td>
        <td>P7638e543-9b7e-4d5d-ab7b-0913878bad03</td>
    </tr>
    <tr>
        <td>open_test_uuid</td>
        <td>UUID konkrétního testu</td>
        <td>String</td>
        <td>Of85bac7c-7c4c-47ab-afdc-eac528639122</td>
    </tr>
    <tr>
        <td>time</td>
        <td>Čas spuštění testu v časové zóně UTC</td>
        <td>String</td>
        <td>2015-05-01 00:34</td>
    </tr>
    <tr>
        <td>cat_technology</td>
        <td>Kategorie použité technologie např. “3G”, “4G”, “WLAN”.</td>
        <td>String</td>
        <td>4G</td>
    </tr>
    <tr>
        <td>network_type</td>
        <td>Typ sítě např. MOBILE, LTE, WLAN.</td>
        <td>String</td>
        <td>LTE</td>
    </tr>
    <tr>
        <td>lat</td>
        <td>Zeměpisná šířka pozice klienta.</td>
        <td>Numeric</td>
        <td>49.25809158659175</td>
    </tr>
    <tr>
        <td>long</td>
        <td>Zeměpisná délka pozice klienta</td>
        <td>Numeric</td>
        <td>17.758191230599575</td>
    </tr>
    <tr>
        <td>loc_src</td>
        <td>Zdroj informací o poloze. Hodnoty: “gps”, “network”.</td>
        <td>String</td>
        <td>gps</td>
    </tr>
    <tr>
        <td>loc_accuracy</td>
        <td>Odhad přesnosti polohy (rádius v metrech)</td>
        <td>Numeric</td>
        <td>6.0</td>
    </tr>
    <tr>
        <td>zip_code</td>
        <td>PSČ klienta, pokud je známé</td>
        <td>Numeric</td>
        <td></td>
    </tr>
    <tr>
        <td>download_kbit</td>
        <td>Rychlost stahování, kilobity za sekundu.</td>
        <td>Numeric</td>
        <td>22180</td>
    </tr>
    <tr>
        <td>upload_kbit</td>
        <td>Rychlost nahrávání, kilobity za sekundu.</td>
        <td>Numeric</td>
        <td>22409</td>
    </tr>
    <tr>
        <td>ping_ms</td>
        <td>Medián pingu (round-trip time) v milisekundách, měřeno na straně serveru.</td>
        <td>Numeric</td>
        <td>31.912917</td>
    </tr>
    <tr>
        <td>lte_rsrp</td>
        <td>Síla LTE signálu v dBm.</td>
        <td>Numeric</td>
        <td></td>
    </tr>
    <tr>
        <td>server_name</td>
        <td>Jméno testovacího serveru.</td>
        <td>String</td>
        <td>CZ.NIC</td>
    </tr>
    <tr>
        <td>test_duration</td>
        <td>Doba trvání testu v sekundách</td>
        <td>Numeric</td>
        <td>7</td>
    </tr>
    <tr>
        <td>num_threads</td>
        <td>Počet vláken testu stahování</td>
        <td>Numeric</td>
        <td>3</td>
    </tr>
    <tr>
        <td>platform</td>
        <td>"Platforma, na které byl test proveden. Např. “Android” (= Google Android App), “iOS” (= Apple iOs App)"</td>
        <td>String</td>
        <td>Android</td>
    </tr>
    <tr>
        <td>model</td>
        <td>Název použitého zařízení</td>
        <td>String</td>
        <td>SM-G920F</td>
    </tr>
    <tr>
        <td>client_version</td>
        <td>Použitá verze klientské aplikace</td>
        <td>String</td>
        <td>2.0.41-CZ_0.9.10-master</td>
    </tr>
    <tr>
        <td>network_mcc_mnc</td>
        <td>Identifikace sítě přičemž první tři čísla reprezentují zemi a čísla za pomlčkou operátora v rámci země.</td>
        <td>String</td>
        <td>230-03</td>
    </tr>
    <tr>
        <td>network_name</td>
        <td>Zobrazované jméno sítě</td>
        <td>String</td>
        <td>Vodafone CZ</td>
    </tr>
    <tr>
        <td>sim_mcc_mnc</td>
        <td>Identifikace sítě poskytovatele dle SIM karty. Význam čísel je stejný jako u network_mcc_mnc</td>
        <td>String</td>
        <td>230-03</td>
    </tr>
    <tr>
        <td>nat_type</td>
        <td>Typ připojená vzhledem k NATu. Např veřejná IP, privátní adresa za NATem…</td>
        <td>String</td>
        <td>nat_local_to_public_ipv4</td>
    </tr>
    <tr>
        <td>asn</td>
        <td>Číslo autonomního systému</td>
        <td>Numeric</td>
        <td>16019</td>
    </tr>
    <tr>
        <td>ip_anonym</td>
        <td>Anonymizovaná IP adresa klienta.</td>
        <td>String</td>
        <td>46.135.48</td>
    </tr>
    <tr>
        <td>ndt_download_kbit</td>
        <td>Rychlost stahování NDT-testu v kilobitech za sekundu.</td>
        <td>Numeric</td>
        <td></td>
    </tr>
    <tr>
        <td>ndt_upload_kbit</td>
        <td>Rychlost nahrávání NDT-testu v kilobitech za vteřinu.</td>
        <td>Numeric</td>
        <td></td>
    </tr>
    <tr>
        <td>implausible</td>
        <td>Identifikace závadných měření</td>
        <td>Boolean</td>
        <td>false</td>
    </tr>
    <tr>
        <td>signal_strength</td>
        <td>Síla signálu (RSSI) v dBm.</td>
        <td>Numeric</td>
        <td>-80</td>
    </tr>
    <tr>
        <td>cell_id</td>
        <td>ID připojené buňky</td>
        <td>Numeric</td>
        <td>329483</td>
    </tr>
    <tr>
        <td>lac</td>
        <td>LAC</td>
        <td>Numeric</td>
        <td>38070</td>
    </tr>
    <tr>
        <td>tags</td>
        <td>Důvody vyřazení ze statistik</td>
        <td>String</td>
        <td>{111,361,362,401}</td>
    </tr>
</table>

### Důvody vyřazení dat ze statistik

<table class="opendata-codes table">
    <thead>
        <tr>
            <th>Kód</th>
            <th>Význam</th>
        </tr>
    </thead>
    <tr>
        <td>110</td>
        <td>měření s chybějícím <code>network_group_type</code> (chybějící hodnota v tabulce <code>group_typ</code>)</td>
    </tr>
    <tr>
        <td>111</td>
        <td>měření s chybějícím <code>network_group_type</code> (existující hodnota v tabulce <code>group_typ</code>)</td>
    </tr>
    <tr>
        <td>200</td>
        <td>Nedokončené měření</td>
    </tr>
    <tr>
        <td>311</td>
        <td>2G příliš vysoká rychlost stahování</td>
    </tr>
    <tr>
        <td>312</td>
        <td>2G příliš vysoká rychlost uploadu</td>
    </tr>
    <tr>
        <td>313</td>
        <td>3G příliš vysoká rychlost stahování</td>
    </tr>
    <tr>
        <td>314</td>
        <td>3G příliš vysoká rychlost uploadu</td>
    </tr>
    <tr>
        <td>315</td>
        <td>4G příliš vysoká rychlost stahování</td>
    </tr>
    <tr>
        <td>316</td>
        <td>4G příliš vysoká rychlost uploadu</td>
    </tr>
    <tr>
        <td>34x</td>
        <td>rychlost neodpovídá přeneseným bajtům</td>
    </tr>
    <tr>
        <td>35x</td>
        <td>přenesené bajty jsou iregulární</td>
    </tr>
    <tr>
        <td>36x</td>
        <td>doba měření není stažená</td>
    </tr>
    <tr>
        <td>401</td>
        <td>vzdálenost mezi dvěma měřeními je příliš vysoká</td>
    </tr>
    <tr>
        <td>411</td>
        <td>měření je příliš daleko od <code>nw_area_code</code> souřadnic</td>
    </tr>
    <tr>
        <td>444</td>
        <td>agregované měření</td>
    </tr>
</table>
