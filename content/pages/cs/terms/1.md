# 1. Úvodní ustanovení

## 1.1

Aplikace NetMetr (dále jen **„Aplikace“**), skládající se ze serverové části a aplikací pro operační systém Android a iOS, je určena k měření rychlosti a kvality mobilního datového spojení. Provoz Aplikace a její další vývoj, zpracování osobních údajů uživatelů Aplikace, sběr dat získaných prostřednictvím Aplikace, jejich zpracování a zveřejňování zajišťuje sdružení CZ.NIC, z.s.p.o., IČ 67985726. Kontrolu metodiky sběru a zpracování dat prostřednictvím Aplikace a jejich kontrolu zajišťuje Český telekomunikační úřad.

## 1.2

Kompletní zdrojový kód Aplikace je k dispozici jako open source na základě příslušných licencí na adrese <https://gitlab.nic.cz/labs/netmetr>.

## 1.3

Název a logo Aplikace jsou autorským dílem ve smyslu zákona č. 121/2000 Sb., autorský zákon, přičemž vykonavatelem autorských práv je sdružení CZ.NIC, z. s. p. o. Nevztahuje se na ně tedy možnost užití na základě principu open source a open data.

## 1.4

Na adresách <www.netmetr.cz>, <www.measurementlab.net> a v mobilních aplikacích jsou k dispozici data získaná prostřednictvím Aplikace, která jsou poskytnuta jako otevřená data na základě licence [CC BY 3.0 CZ](http://creativecommons.org/licenses/by/3.0/cz/), případně na základě jiné obdobné licence. Otevřená data jsou poskytována veřejnosti k dalšímu zpracování. Data získaná prostřednictvím Aplikace, která nejsou na uvedených adresách nebo v mobilních aplikacích k dispozici, nejsou veřejně dostupná jako otevřená data.

## 1.5

Aplikaci lze užívat pouze na základě výslovného souhlasu uživatele s těmito podmínkami, přičemž tento souhlas může být kdykoliv odvolán. Data získaná během doposud provedených měření zůstávají i po odvolání souhlasu součástí záznamů, součástí dat nejsou osobní údaje uživatele. Souhlas uživatele je zaznamenáván. V případě, že dojde ke změně ve zpracování nebo nakládání s daty získávanými prostřednictvím Aplikace, zejména v souvislosti s novými verzemi Aplikace), musí uživatel opětovně výslovně udělit souhlas s těmito podmínkami.

## 1.6

Aplikace umožňuje provedení dvou samostatných testů, přičemž pro některé platformy (operační systémy) nemusí být všechny testy dostupné. Základní test měří rychlosti downloadu, uploadu a další kvalitativní parametry datového spojení. Druhým testem je volitelný NDT test, který měří další technické parametry. Tento test je k dispozici pouze v případě, že to umožňuje platforma (operační systém) na zařízení uživatele.

## 1.7

Sdružení CZ.NIC oznámilo Úřadu pro ochranu osobních údajů zpracovávání údajů dle těchto podmínek a ve veřejném registru zpracování osobních údajů je vedeno pod č. 00021060, poř. č. 006.
