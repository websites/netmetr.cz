lang: en
title: Open Data
slug: open-data
slug_cs: open-data
template: data
sortorder: 98
older_data_heading: Download Open Data
older_data_button: Download

Open Data - Export of measurement results
----------------------------------

Measurement results are available to all interested parties as Open Data licensed under the [Creative Commons Attribution 4.0 (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.en).

A data format specification is available [here](open-data-specs.html).

Publication of the data is in accordance with the privacy policy.

<small>Note: The timestamps are in UTC (not local time).</small>

[![★★★](/theme/images/data-badge-3.png)](http://5stardata.info/en/)
