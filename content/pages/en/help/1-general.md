## 1. General questions

### 1.1 What is NetMetr?

NetMetr serves for thorough testing of your connection to the Internet over cellular networks and Wi-Fi.

NetMetr offers:

- Mobile connection detailed testing
- The display of test results on the map with the possibility of filtering according to various parameters
- The measurement result statistics in networks of all operators
- Ability to synchronize the results of tests of various equipment and their common display
- History of your tests
- The data obtained from the measurement and the application source codes are published as OpenData and OpenSource

### 1.2 Who is the author of NetMetr?

NetMetr is based on the source code of RTR-NetTest ([netztest.at](https://www.netztest.at)), which
is an open source tool from the [Austrian Regulatory Authority for
Broadcasting and Telecommunications (RTR)](https://www.rtr.at/).

NetMetr itself is developed by [CZ.NIC](https://www.nic.cz/) and the source code is available [here](https://gitlab.nic.cz/labs/netmetr).

### 1.3 Why should I test my connection?

Bandwidth is an important quality parameter for your mobile Internet connection. NetMetr offers you the opportunity to check the current bandwidth. Another possibility is to compare the test results with those of other users. On the Android system it supports network analysis, provides a more detailed test tool for network diagnostics (NDT). The test results carried out by all operators in various places may be of interest to a large number of users.

### 1.4 What is being tested?

Pomocí NetMetru můžete ve svém mobilním zařízení otestovat:

- download speed,
- upload speed,
- connection latency (ping),
- signal strength, if the device allows it

The additional test (NDT), which is available on Android devices, can test also other quality parameters, such as packet loss.

### 1.5 How to contact us?

You can contact Technical support, mainly with the questions for which you have not found the answer here. Please send your questions to <mailto:netmetr@labs.nic.cz>.

### 1.6 What types of connections can be tested?

NetMetr is available for mobile devices running on Android and iOS (Apple). You can test both over cellular networks (EDGE, UMTS, LTE) and Wi-Fi.

In case of computers or mobile devices with an unsupported operating system, you can use the Web version of the test, functional in all modern web browsers.

### 1.7 How to contact technical support if I have a problem with NetMetr?

Before contacting technical support please try first to find an answer to your question in this FAQ. If you don’t find the answer, we would be happy if you write us to <mailto:netmetr@labs.nic.cz>.

### 1.8 What is the difference between NetMetr and other applications for measuring the speed of the connection?

NetMetr offers:

- independent and non-commercial solution
- metering server connected through a fast and high capacity connection to [NIX.CZ](https://www.nix.cz)
- detailed analysis of the connection using NDT test
- open data – data, obtained from measurements, are available to the public for further analysis as Open Data
