## 2. Technology

### 2.1 How does NetMetr work?

NetMetr measures the qualitative parameters of your wireless Internet connection by testing transmissions from your mobile device to the server and vice versa. Actual values ​​of connection parameters ​​are calculated from the measured values. A complete description of the test methodology is available [here](https://www.netztest.at/doc/).

NetMetr can perform two different tests:

- **The basic test** is a quick analysis of download and upload speed, latency (ping), and - for Android - signal strength.
- **Detailed test** (NDT test) is an additional independent test of the  [M-Lab association](<http://www.measurementlab.net>), using specific diagnostic options in Android system.
- In addition, in Android also *Quality of Service* properties are tested. (changes of content, reference page, transparency of connection, DNS, TCP and UDP ports).

### 2.2 What does the NDT test do?

After performing the quick baseline test of your connection you can run the additional test for complex network analysis. The Network Diagnostic Tool (NDT) is used, from M-Lab association (<http://www.measurementlab.net>).IOS (iPhone, iPad) does not support this test, unfortunately.

Systém iOS (iPhone, iPad) provádění tohoto testu bohužel neumožňuje.

### 2.3 What are the benefits of complementary NDT test?

This more detailed test verifies the amount of network parameters that are necessary for a comprehensive analysis of your connection and are not tested by the basic test. A list of all network parameters and details of the NDT test can be found on the  [M-Lab web](https://code.google.com/p/ndt/w/list).

### 2.4 How is my Internet connection tested?

NetMetr measures the connection from your device to the test server. This server is connected by a fast and high capacity connection directly to the Internet [NIX node](https://en.wikipedia.org/wiki/Neutral_Internet_Exchange_of_the_Czech_Republic), thereby minimizing the effect of the data transport speed on the servers of your mobile operator.

### 2.5 What should you pay attention to before running the test?

To obtain the least distorted test results you must meet several conditions:

- There are no data intensive applications running in the background on your device.
- The device is as close to a Wi-Fi AP as possible to avoid distorting the test with a weak signal (when measuring connectivity via Wi-Fi)
- You did not run over your FUP and the operator did not reduce the speed of your data connection (when measuring mobile data connection)

### 2.6 Is the source code available?

NetMetr is an open source, the source code of the server part is available at <https://gitlab.nic.cz/labs/netmetr>, the mobile apps at <https://gitlab.nic.cz/mobile/netmetr_flutter>.

### 2.7 What connection speed do I need?

For some applications a large bandwidth is not required. For normal surfing, 2 Mb/s  are usually quite sufficient. However, some applications do not work properly without sufficient bandwidth. For example, online video and music can be operated without interruption only if the connection is fast enough.

It is important to take into account not only the bandwidth towards the user (download), but also towards the Internet (upload).

### 2.8 What factors influence the test result?

- Maximum bandwidth of mobile connection to the Internet (speed limit depending on the tariff plan)
- Bandwidth provided by your internet provider, its utilization
- The device on which the test is conducted
- Operating system of the device and its configuration

When using Wi-Fi:

- The distance between the Wi-Fi router and your device
- Quality of Wi-Fi router and the supported technology (eg. 802.11n or 802.11ac)

When using a mobile connection:
- The technology used (EDGE, UMTS, LTE)
- Quality of service at the point of measurement (signal strength, depending on the device)
- Whether you are inside or outside the building,
- How fast you are moving (in a moving car, on a highway or in a train, you will have a lower bit rate)
- The number of users simultaneously sharing the base station (BTS)

### 2.9 What is the optimal day time for the test?

The test result is inter alia influenced by the day time. It is therefore recommended to perform a test at regular intervals and at different times of the day. The use of Internet, depending on the day time, can be estimated by [use of the connection to the main Czech Internet NIX node](https://www.nix.cz/ports/aggr/day).

### 2.10 What is the influence of my Wi-Fi router or access point on the measurement?

Wi-Fi router or AP can have a huge impact on the speed of connection.

If your router is unable to utilize the full capacity of your Internet connection, it will affect the test results. For high-speed connectivity it is always advisable to use a router that supports IEEE 802.11n or 802.11ac, of course only if the connected devices support these standards.

It is also important that the router can perform Network Address Translation (NAT) with sufficiently high throughput. Also, the distance between the router and your device should not be too big.

If the test result is fundamentally different from the expected values, we recommend checking the wireless router.

### 2.11 How many tests need to be done to get really reliable values?

The test result depends on several factors: day time, network usage and your location. You can increase the predictive value of your test results by rerunning them at different times and different places. The more tests you run, the better basis for an objective assessment of your connection you get.

### 2.12 How often can I repeat testing with NetMetr?

The NetMetr test can be repeated as often as you want. For example, in mobile applications the test can be repeated immediately after its completion, just click on the "New test".

Before repeating it is necessary to complete the detailed test (NDT applicable only for Android).

**The test generates a transfer of large volumes of data** - if you test over cellular networks, you can exceed your FUP limits and a subsequent speed reduction by the operator can happen.

Regular tests will improve the results accuracy.

### 2.13 What are the volumes of data transmitted in the test?

The amount of transmitted data varies depending on the bandwidth of the Internet connection. NetMetr tests the download and upload capacity of your Internet connection for about 20 seconds, with the fastest available speed. Depending on the available bandwidth of the connection it may result in an increased utilization of data.

- When measuring via GPRS the test transfers approx. 130 kB data, over HSDPA the test is essentially more data demanding, it consumes over 14 MB of data.
- With a very fast internet connection NetMetr uses very large amounts of data, on a symmetrical 100 Mb/s connection it consumes a data volume of approx. 250 megabytes
- If you carry out more detailed testing using NDT test data volume will increase approximately 2.5 times.

Please note that the data transmission can cause an exhaustion of the FUP when measuring a connection via the cellular network, depending on your tariff plan.

### 2.14 What version of IP protocol is used?

By default, the application uses IPv6, if available. If not available,  the IPv4 is used. You can disable IPv6 in the application (the "Options").

For more information about IPv6, visit the [Wikipedia article on IPv6](https://en.wikipedia.org/wiki/IPv6).
