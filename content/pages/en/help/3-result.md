## 3. The results of the tests

### 3.1 What “download” means?

Download is a download of data from the Internet to your device. The unit “Mbit/s” is the number of megabits per second.

Data speed, advertised by Internet service providers, usually means an “absolute” data speed, incorporating also the transmission of other data, due to the overhead network protocol used. NetMetr however measures the “real” connection speeds. These values ​​are on average about 10% lower than the absolute transmission speed of your Internet connection.

### 3.2 What “upload” means?

Upload means the transfer of data from your device to the Internet. Upload speeds normally do not appear in marketing materials and are usually much lower than the download speed. But for fast access to the internet it has similar importance as download speeds, because the communication on the Internet is carried out practically always bidirectionally. Speed ​​of data upload is especially important if you want to send photos, use technology to share files or use video calls.

As with download, there is a difference between "absolute" and "real" data transfer rate here too.

### 3.3 What is the latency (ping)?

Latency (ping) indicates the server's response time - the time it takes to transfer small amounts of data from your device to the server and back. It is measured in milliseconds (ms). Latency is very important not only for playing online games, but even for completely normal surfing high latency has significant impact on user comfort. This delay is significantly affected by the technology used for your Internet connection, as well as its use (eg. how many users are currently connected simultaneously).

Fast connections to the Internet have a latency typically less than 2 ms (fast copper networks and fiber networks), slow connections can have a latency of more than 500 ms (GSM); typical values are in the range from 10 to 50 ms. Of course it depends on how the server is connected. If you are connecting to a remote server, the length of the transmission connection has also a significant impact on latency (eg. for Japan it may be from Europe about 280 ms even on fast connections.).


### 3.4 What is the strength of the signal?

The stronger the signal that your device receives from the base station (or access point of Wi-Fi), the faster and more stable internet connection you can expect. If your cell phone is outdoor or near an open window, the signal is usually better than indoors. In case of connection via Wi-Fi, it is important that as few obstacles as possible shadow to signal (eg. walls) between Wi-Fi access point and the device (eg. tablet).

### 3.5 How to read the signal strength?

Signal strength (in dBm) is measured in negative numbers. The higher the measured value is (closer to zero), the stronger the signal. For example, the value of -50 dBm is a very good signal, the value of -113 dBm indicates the GSM / UMTS signal is very weak.

### 3.6 How do I know if the test result is good or bad?

Color of the icon (red/yellow/green) varies by the values of data speed signals:

- <span class="classification inline" data-classification="3"></span> **Green:** The value is 10&nbsp;Mb/s or higher (download), respectively 2&nbsp;Mb/s or higer (upload)
- <span class="classification inline" data-classification="2"></span> **Yellow:** The value is between 2 and 10&nbsp;Mb/s (download), respectively between 1 a 2&nbsp;Mb/s (upload)
- <span class="classification inline" data-classification="1"></span> **Red:** The value is lower than 2&nbsp;Mb/s (download), respectively lower than 1&nbsp;Mb/s (upload)

This color range is displayed regardless of the type the connection and corresponds solely to the measured velocities. Very high speeds can be achieved only through certain technologies such as HSPA + or LTE. Therefore, a red icon at the result does not necessarily mean a bad result, if in a testing area is available only 2G network.

For ping, the icon color varies by the network latency:

- <span class="classification inline" data-classification="3"></span> **Green:** The value is 25&nbsp;ms or lower
- <span class="classification inline" data-classification="2"></span> **Yellow:** The value is between 25 and 75&nbsp;ms
- <span class="classification inline" data-classification="1"></span> **Red:** The value is 75&nbsp;ms or higher

### 3.7 How to properly interpret test results?

The measurement result shows a snapshot of the quality of your Internet access. **If test results are lower than those advertised by the operator, it does not necessarily mean that operator fails to comply with terms and conditions.**

The network connection may be affected by various technological factors as the number of other users who are simultaneously connected to the same base station of mobile networks, quality of Wi-Fi router, system configuration, etc.

In any case you can reach really reliable results only if you frequently repeat measurements while you consider possible systematic and random measurement errors.

### 3.8 What is the unit *Mb/s*?

Data connection speed is usually given in Mb/s (eg. 8 Mb/s), which stands for megabits per second. Following conversions are used:

- 1 Mb/s = 1000 kbit/s
- 1 kbit/s = 1000 b/s

In addition to using the unit b/s it is customary to use also byte/s (B/s, e.g. information about the download speed in Internet browsers): Since 1 byte/sec = 8 b/s, the speed Internet connection can be described as 8 Mb/s, 8000 kbit/s or 1 MB/s.

Download of 3 MB file on 8 Mb/s connection will take about 3 seconds.

### 3.9 Is there any operator ranking by speed?

It is not the purpose of the application NetMetr to rank operators, but rather to provide users with comprehensive information on the quality of their Internet connection. Statistics from all the test results, broken down by operators, can be found the  [Stats](stats.html) page or divided by individual months on the web page [Monthly results](monthly-results.html).

When assessing the operators offers please keep always in mind that declared values ​​are usually the maximum theoretically possible, and that the real bandwidth is often much lower.


### 3.10 MCan I run the test, although the NDT test is not finished yet?

No, the two tests cannot be performed simultaneously.

### 3.11 Can a bad test result be caused by exceeding the monthly limit of the volume of transferred data (FUP)?

Yes, this is a very common problem. FUP, limit of the volume of data transferred, applies within the vast majority of mobile tariffs. After a certain amount of data transferred (both directions) operator reduces the speed of the connection. This limit varies depending on your operator and your tariff.

In the case that this restriction is applied by your operator, it will naturally have a negative influence on the measured speed value.

### 3.12 What is the synchronization code?

Test results from a variety of devices can be synchronized to have summary from other connected devices in each of the applications. For this purpose, the user must obtain the synchronization code from the NetMetr application and enter it in the appropriate devices.

Synchronization code can be entered (and display the results) also on the [My measurements](my.html) page.

### 3.13 I have problems with the measurements of signal strength

Some devices may display incorrect or outdated signal strength of the cellular network (it was observed in some Samsung Galaxy S2 phones, among others). In such cases the measurement of signal strength is not displayed in the application.

### 3.14 What is QoS?

QoS stands for Quality of Service. It is a set of rules how to set for certain data the maximum / minimum transmission speed, to prefer some traffic, and the like. More on [Wikipedia](https://en.wikipedia.org/wiki/Quality_of_Service).
