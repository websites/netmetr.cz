## 4. The map

### 4.1 What results are displayed on the map?

The map displays the measurement results of the standard test, whereas the more detailed test results of optional NDT are not on the map, but are only available as tabular data. Map, in the mobile application or in the browser, offers a possibility by means of filters to select which specific data will be displayed on the map.

### 4.2 Does the map show the results of measurements performed outside the Czech Republic?

Yes, the results appear from around the world. Measurement is not geographically limited.

### 4.3 What is a percentile and a median?

The percentile is a statistical indicator describing value, below which fall X % of all measured values. For example, the 20% percentile is the value below which fall 20% of all test results.

The median is the 50% percentile, so it is a value that divides all measured values ​​into two equal sets. In other words, there is the same number of measurements with a value less as well as with a value larger than the median.

### 4.4 How exactly are placed locations with measured values?

Localization measurement is performed during the measurement depending on the capabilities and settings of your device and using

- GPS,
- Information from the network (Wi-Fi or cellular)
- Or by IP address

Localization with GPS is the most exact way, while using a mobile network we only get the approximate information on location. Determining position using an IP address is often highly inaccurate.

Measurements with an accuracy of less than 2 km are not displayed on the map. Measurements with an accuracy of less than 10 km are not presented in detail. We therefore recommend before measuring to activate GPS (if available).

Data on the accuracy of specific measurements are available as part of the [open data](open-data.html).
