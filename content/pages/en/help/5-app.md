## 5. Application for Mobile Devices

### 5.1 What permissions are required by the NetMetr application?

When installing the application, it requires the following permissions:

- Location - approximate location (by means of network) or precise location (by means GPS and network), it allows for example to locate the test result on the map
- Photos / media / files - this permission is required to store maps into memory
- Hardware controls - to prevent the device from going into sleep mode - measurement could be distorted if the device goes into sleep mode during the test
- Information about Wi-Fi connectivity

### 5.2 Which operating systems are supported?

Android and Apple iOS. On unsupported operating systems you can use the [web test](test.html).

### 5.3 Which versions of Android are supported?

Android version 4.0.3 and higher.

### 5.4 Which versions of iOS are supported?

IOS version 7 and higher.

### 5.5 What is important to know before using?

- If you have a tariff plan with a limit for the volume of traffic (ie. FUP), use NetMetr for measuring cellular connection carefully. Within one test a relatively high volume of data is transmitted (actual volume depends primarily on the type of connection). This problem of measurement is usually not relevant when connected to a WiFi network.
- Certain factors (such as time of day, position inside / outside buildings, signal strength, etc.), have big impact on the test result in mobile networks
- After exceeding monthly data limit the mobile operator will reduce the speed of the connection. This obviously affects the test results.

### 5.6 Is it possible to use NetMetr on tablets?

NetMetr works on tablets and is optimized for them.

### 5.7 How to install Android application?

You can find NetMetr on [Google Play](https://play.google.com/store/apps/details?id=cz.nic.netmetr).

### 5.8 How to install iOS app?

You can find NetMetr on [Apple AppStore](https://itunes.apple.com/cz/app/netmetr/id946478662).
