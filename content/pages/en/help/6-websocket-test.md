## 6. Web test

### 6.1 Which browsers are supported?

Web NetMetr test works on all versions of major browsers supported by their manufacturers. To perform the test it is necessary to have functional [support of  the Web Sockets](http://caniuse.com/#feat=websockets) technology. An overview of  supported browser versions can be found on Wikipedia:

- [Google Chrome](https://en.wikipedia.org/wiki/Google_Chrome_release_history),
- [Mozilla Firefox](https://en.wikipedia.org/wiki/Firefox_release_history#Release_history),
- [MS Internet Explorer](https://en.wikipedia.org/wiki/Internet_Explorer_versions#Release_history_for_desktop_Windows_OS_version).

### 6.2 What aspects does the web test measure?

Just download, upload and ping.

Advanced measurements of QoS and network parameters by NDT test is available only on the Android application.

### 6.3 Do you need to install some plugins to browser?

You do not. You need only a modern browser with [Web Sockets support](http://caniuse.com/#feat=websockets).

### 6.4  Why the browser asks for my location in the beginning of the test?

The test results, when location is detected with sufficient accuracy, are shown on the [map](map.html). If you don’t want to publish your location, you don’t have to - the test result is not affected.

Determining the location without the GPS may not be very accurate, results with big uncertainty are not displayed on the map.

### 6.5 Is it possible to display the results of my web and mobile test together?

Yes it is. You should generate the synchronization code on the application on the mobile device and enter it on the [My measurement](my.html) page. Then you can find the tests from your mobile application in the table.
