lang: en
title: Home
slug: index
template: index
sortorder: 0
icon_app: Mobile apps<p>Download an app for your Android or iOS device.</p>
icon_map: Map<p>Browse through a map with filterable results.</p>
icon_stats: Stats<p>Overview of measurement results.</p>
terms_link_text: Terms of use
terms_link_url: terms.html

NetMetr is a tool for measuring the actual quality of Internet access services, such as download and upload speed, latency, and, for radio networks, also signal strength. It allows users to perform thorough and detailed testing and get comprehensive information not only about the status of their particular connection, but also analyze previous measurements and especially measurements of other participants. All data (excluding personal data) and source codes are released on the Open Source principle.
