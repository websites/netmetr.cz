lang: en
title: My measurements
slug: my
slug_cs: moje
template: my-measurements
sortorder: 5
table_heading: Measurement history
sync_heading: Combine results from multiple devices

Test results from a variety of devices can be synchronized and displayed together. Pairing is performed by entering synchronization code that you can generate in the application on the relevant device.
