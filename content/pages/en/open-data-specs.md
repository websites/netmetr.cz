lang: en
title: Open data – Format specification
slug: open-data-specs
slug_cs: open-data-specifikace
sortorder: -1

<table class="opendata table">
    <thead>
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Type</th>
            <th>Example</th>
        </tr>
    </thead>
    <tr>
        <td>open_uuid</td>
        <td>Identification of a client who started the test</td>
        <td>String</td>
        <td>P7638e543-9b7e-4d5d-ab7b-0913878bad03</td>
    </tr>
    <tr>
        <td>open_test_uuid</td>
        <td>Unique identification of a specific test</td>
        <td>String</td>
        <td>Of85bac7c-7c4c-47ab-afdc-eac528639122</td>
    </tr>
    <tr>
        <td>time</td>
        <td>Time when the test started, in UTC time</td>
        <td>String</td>
        <td>2015-05-01 00:34</td>
    </tr>
    <tr>
        <td>cat_technology</td>
        <td>Category of technology used e.g. “3G”, “4G”, “WLAN”</td>
        <td>String</td>
        <td>4G</td>
    </tr>
    <tr>
        <td>network_type</td>
        <td>Network type e.g. MOBILE, LTE, WLAN</td>
        <td>String</td>
        <td>LTE</td>
    </tr>
    <tr>
        <td>lat</td>
        <td>Latitude of the client’s position</td>
        <td>Numeric</td>
        <td>49.25809158659175</td>
    </tr>
    <tr>
        <td>long</td>
        <td>Longitude of the client’s position</td>
        <td>Numeric</td>
        <td>17.758191230599575</td>
    </tr>
    <tr>
        <td>loc_src</td>
        <td>Source of location information. Values: “gps”, “network”.</td>
        <td>String</td>
        <td>gps</td>
    </tr>
    <tr>
        <td>loc_accuracy</td>
        <td>Estimation of position accuracy (radius in meters)</td>
        <td>Numeric</td>
        <td>6.0</td>
    </tr>
    <tr>
        <td>zip_code</td>
        <td>Client postal code, if known</td>
        <td>Numeric</td>
        <td></td>
    </tr>
    <tr>
        <td>download_kbit</td>
        <td>Download speed, kilobits per second</td>
        <td>Numeric</td>
        <td>22180</td>
    </tr>
    <tr>
        <td>upload_kbit</td>
        <td>Upload speed, kilobits per second</td>
        <td>Numeric</td>
        <td>22409</td>
    </tr>
    <tr>
        <td>ping_ms</td>
        <td>Median ping (round-trip time) in milliseconds, as measured on the server side</td>
        <td>Numeric</td>
        <td>31.912917</td>
    </tr>
    <tr>
        <td>lte_rsrp</td>
        <td>LTE signal strength in dBm</td>
        <td>Numeric</td>
        <td></td>
    </tr>
    <tr>
        <td>server_name</td>
        <td>Name of the test server/td>
        <td>String</td>
        <td>CZ.NIC</td>
    </tr>
    <tr>
        <td>test_duration</td>
        <td>Duration of the test, in seconds</td>
        <td>Numeric</td>
        <td>7</td>
    </tr>
    <tr>
        <td>num_threads</td>
        <td>Number of threads of the download test</td>
        <td>Numeric</td>
        <td>3</td>
    </tr>
    <tr>
        <td>platform</td>
        <td>The platform on which the test was performed. Eg. "Android" (= Google Android App), "iOS" (= Apple iOS App)</td>
        <td>String</td>
        <td>Android</td>
    </tr>
    <tr>
        <td>model</td>
        <td>Name/model of the devise used</td>
        <td>String</td>
        <td>SM-G920F</td>
    </tr>
    <tr>
        <td>client_version</td>
        <td>Version of the client application</td>
        <td>String</td>
        <td>2.0.41-CZ_0.9.10-master</td>
    </tr>
    <tr>
        <td>network_mcc_mnc</td>
        <td>Network identification, the first three numbers represent the country and the number after the hyphen represents operator in the country</td>
        <td>String</td>
        <td>230-03</td>
    </tr>
    <tr>
        <td>network_name</td>
        <td>Display Network name</td>
        <td>String</td>
        <td>Vodafone CZ</td>
    </tr>
    <tr>
        <td>sim_mcc_mnc</td>
        <td>Identification of network provider according to the SIM card. Meaning of the numbers is the same as network_mcc_mnc</td>
        <td>String</td>
        <td>230-03</td>
    </tr>
    <tr>
        <td>nat_type</td>
        <td>Type of connection due to NAT. E.g. public IP, private IP address behind NAT, …</td>
        <td>String</td>
        <td>nat_local_to_public_ipv4</td>
    </tr>
    <tr>
        <td>asn</td>
        <td>Autonomous system number</td>
        <td>Numeric</td>
        <td>16019</td>
    </tr>
    <tr>
        <td>ip_anonym</td>
        <td>Anonymised IP address of the client</td>
        <td>String</td>
        <td>46.135.48</td>
    </tr>
    <tr>
        <td>ndt_download_kbit</td>
        <td>Download speed of NDT-test (kilobit per second)</td>
        <td>Numeric</td>
        <td></td>
    </tr>
    <tr>
        <td>ndt_upload_kbit</td>
        <td>Download speed of NDT-test (kilobit per second)</td>
        <td>Numeric</td>
        <td></td>
    </tr>
    <tr>
        <td>implausible</td>
        <td>Upload speed of NDT-test (kilobit per second)</td>
        <td>Boolean</td>
        <td>false</td>
    </tr>
    <tr>
        <td>signal_strength</td>
        <td>Signal strength (RSSI) in dBm</td>
        <td>Numeric</td>
        <td>-80</td>
    </tr>
    <tr>
        <td>cell_id</td>
        <td>ID of connected cell</td>
        <td>Numeric</td>
        <td>329483</td>
    </tr>
    <tr>
        <td>lac</td>
        <td>LAC</td>
        <td>Numeric</td>
        <td>38070</td>
    </tr>
    <tr>
        <td>tags</td>
        <td>Reasons for exclusion from the statistics</td>
        <td>String</td>
        <td>{111,361,362,401}</td>
    </tr>
</table>

### Reasons for exclusion from the statistics

<table class="opendata-codes table">
    <thead>
        <tr>
            <th>Code</th>
            <th>Meaning</th>
        </tr>
    </thead>
    <tr>
        <td>110</td>
        <td>Measurement with missing network_group_type (missing value in table <code>group_typ</code>)</td>
    </tr>
    <tr>
        <td>111</td>
        <td>Measurement with missing network_group_type (missing value in table <code>group_typ</code>)</td>
    </tr>
    <tr>
        <td>200</td>
        <td>Unfinished measurement</td>
    </tr>
    <tr>
        <td>311</td>
        <td>2G too high download speed</td>
    </tr>
    <tr>
        <td>312</td>
        <td>2G too high upload speed</td>
    </tr>
    <tr>
        <td>313</td>
        <td>3G too high download speed</td>
    </tr>
    <tr>
        <td>314</td>
        <td>3G too high upload speed</td>
    </tr>
    <tr>
        <td>315</td>
        <td>4G too high download speed</td>
    </tr>
    <tr>
        <td>316</td>
        <td>4G too high upload speed</td>
    </tr>
    <tr>
        <td>34x</td>
        <td>Speed does not match the transferred bytes</td>
    </tr>
    <tr>
        <td>35x</td>
        <td>Transferred bytes are irregular</td>
    </tr>
    <tr>
        <td>36x</td>
        <td>Measurement of time is not downloaded</td>
    </tr>
    <tr>
        <td>401</td>
        <td>Distance between the two measurements is too high</td>
    </tr>
    <tr>
        <td>411</td>
        <td>Measurement is too far from the <code>nw_area_code</code> coordinates</td>
    </tr>
    <tr>
        <td>444</td>
        <td>aggregate measurement</td>
    </tr>
</table>
