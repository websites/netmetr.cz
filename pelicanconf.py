#!/usr/bin/env python
# -*- coding: utf-8 -*- #

from __future__ import unicode_literals
from webassets.filter import Filter, register_filter
import re

DELETE_OUTPUT_DIRECTORY = True
OUTPUT_PATH = "output/"

AUTHOR = "CZ.NIC Labs"
SITENAME = "NetMetr"
SITEURL = "https://www.netmetr.cz/"
ROOTURL = "https://www.netmetr.cz/"
COOKIE_NAME = "netmetr_uuid"

SITE_LOGO = ROOTURL + "theme/images/netmetr-logo.svg"

PATH = "content"

TIMEZONE = "Europe/Prague"
DEFAULT_LANG = "cs"

languages_lookup = {
    "en": "English",
    "cs": "Čeština"
}

I18N_SUBSITES = {
    "cs": {
        "FOOTER_LINKS": [
            {
                "name": "FRED",
                "url": "http://fred.nic.cz/"
            },
            {
                "name": "BIRD",
                "url": "http://bird.network.cz/"
            },
            {
                "name": "Knot DNS",
                "url": "https://www.knot-dns.cz/"
            },
            {
                "name": "CSIRT",
                "url": "https://www.csirt.cz/"
            },
            {
                "name": "Turris",
                "url": "https://www.turris.cz/"
            },
            {
                "name": "Turris Omnia",
                "url": "https://omnia.turris.cz/"
            },
            {
                "name": "Skener webu",
                "url": "https://www.skenerwebu.cz/"
            }
        ]
    },
    "en": {
        "FOOTER_LINKS": [
            {
                "name": "FRED",
                "url": "http://fred.nic.cz/"
            },
            {
                "name": "BIRD",
                "url": "http://bird.network.cz/"
            },
            {
                "name": "Knot DNS",
                "url": "https://www.knot-dns.cz/"
            },
            {
                "name": "CSIRT",
                "url": "https://www.csirt.cz/"
            },
            {
                "name": "Turris",
                "url": "https://www.turris.cz/"
            },
            {
                "name": "Turris Omnia",
                "url": "https://omnia.turris.cz/"
            },
            {
                "name": "Web scanner",
                "url": "https://www.skenerwebu.cz/"
            }
        ]
    }
}


STRINGS = {
    "cs": {
        "othersites": "Ostatní weby",
        "testing": "Upozornění: Aplikace NetMetr nyní funguje v&nbsp;testovacím provozu.",
        "home": "Úvod",
        #
        "provider_name": "Název operátora",
        "download": "Download",
        "upload": "Upload",
        "ping": "Ping",
        "signal": "Signál",
        "test_count": "Testy",
        #
        "results": "Výsledky",
        "measurements": "Měření",
        #
        "detail": "Detail měření",
        "test_time": "Čas testu",
        "device": "Zařízení",
        "devices": "Zařízení",
        "provider": "Operátor",
        "providers": "Mobilní operátoři",
        "network": "Síť",
        "technology": "Technologie",
        "connection": "Připojení",
        "app_version": "Verze aplikace",
        "latest_results": "Poslední výsledky",
        #
        "toc": "Obsah",
        #
        "older_results": "Starší výsledky",
        #
        "get_csv": "Stáhnout historii Vašich měření (CSV)",
        "sync_code": "Synchronizační kód",
        "sync_button": "Synchronizovat",
        "sync_error_format": "Nesprávný formát synchronizačního kódu (12 znaků, malá písmena a číslice).",
        "gen_sync": "Můžete si také vygenerovat nový kód pro načtení výsledků zobrazených výše na jiném zařízení:",
        "gen_sync_button": "Vygenerovat kód",
        "your_sync": "Váš synchronizační kód: ",
        "all": "Vše",
        "period": "Období",
        "1m": "1 měsíc",
        "2m": "2 měsíce",
        "3m": "3 měsíce",
        "6m": "6 měsíců",
        "1y": "1 rok",
        "2y": "2 roky",
        #
        "ws_test": "Webový test",
        "start_test": "Spustit test",
        "server_name": "Název serveru",
        "location": "Poloha",
        "status": "Stav",
        #
        "map_osm": "OpenStreetMap Mapnik",
        "map_cartodb": "OpenStreetMap cartoDB",
        "map_heat": "Heatmapa",
        "map_points": "Jednotlivá měření",
        #
        "area_map": "Mapa oblasti",
        #
        "display_tests": "Zobrazit testy",
        "hide_tests": "Skrýt testy",
        #
        "app_html": "aplikace.html",
        "map_html": "mapa.html",
        "stats_html": "statistiky.html",
        #
        "wlan": "Wi-fi",
        "browser": "Prohlížeč",
        #
        "init": "Inicializace",
        "test_failed": "Test selhal.",
        "test_stopped": "Test byl zastaven.",
        #
        "connection_err": "Nepodařilo se spojit se serverem.",
        #
        "dload_green": "Podíl měření nad 10 Mb/s",
        "dload_yellow": "Podíl měření mezi 2 a 10 Mb/s",
        "dload_red": "Podíl měření do 2 Mb/s",
        "upload_green": "Podíl měření nad 2 Mb/s",
        "upload_yellow": "Podíl měření mezi 1 a 2 Mb/s",
        "upload_red": "Podíl měření do 1 Mb/s",
        "ping_green": "Podíl měření do 25 ms",
        "ping_yellow": "Podíl měření mezi 25 a 75 ms",
        "ping_red": "Podíl měření nad 75 ms",
        "stats_table_help": "Tabulka uvádí <a href='napoveda.html#43-co-to-je-percentil-a-median'>medián</a> naměřených hodnot. Význam jednotlivých barev naleznete v <a href='napoveda.html#36-jak-poznam-jestli-je-vysledek-testu-dobry-nebo-spatny'>nápovědě</a>.",
        "service_end": "Provoz projektu NetMetr byl k 30. dubnu 2022 ukončen. Více v <a href='https://www.nic.cz/page/4305/sdruzeni-cznic-ukonci-projekt-netmetr/'>tiskové zprávě</a>."
    },
    "en": {
        "othersites": "Other sites",
        "testing": "Caution: NetMetr is in a testing mode.",
        "home": "Home",
        #
        "results": "Results",
        "measurements": "Measurements",
        #
        "provider_name": "Provider name",
        "download": "Download",
        "upload": "Upload",
        "ping": "Ping",
        "signal": "Signal",
        "test_count": "Tests",
        #
        "detail": "Measurement detail",
        "test_time": "Test time",
        "device": "Device",
        "devices": "Devices",
        "provider": "Operator",
        "providers": "Mobile providers",
        "network": "Network",
        "technology": "Technology",
        "connection": "Connection",
        "app_version": "App version",
        "latest_results": "Latest results",
        #
        "toc": "Contents",
        #
        "older_results": "Older results",
        #
        "get_csv": "Download your measurement history (CSV)",
        "sync_code": "Sync code",
        "sync_button": "Synchronize",
        "sync_error_format": "Bad sync code format (12 characters, lowercase letters and numbers).",
        "gen_sync": "You can also generate a new sync code to get the results above to an another device:",
        "gen_sync_button": "Generate sync code",
        "your_sync": "Your sync code: ",
        "all": "All",
        "period": "Period",
        "1m": "1 month",
        "2m": "2 months",
        "3m": "3 months",
        "6m": "6 months",
        "1y": "1 year",
        "2y": "2 years",
        #
        "ws_test": "Web test",
        "start_test": "start test",
        "server_name": "Server name",
        "location": "Location",
        "status": "Status",
        #
        "map_osm": "OpenStreetMap Mapnik",
        "map_cartodb": "OpenStreetMap cartoDB",
        "map_heat": "Heatmap",
        "map_points": "Individual measurements",
        #
        "area_map": "Map of this area",
        #
        "display_tests": "Display tests",
        "hide_tests": "Hide tests",
        #
        "app_html": "apps.html",
        "map_html": "map.html",
        "stats_html": "stats.html",
        #
        "wlan": "Wi-fi",
        "browser": "Browser",
        #
        "init": "Init",
        "test_failed": "Test has failed.",
        "test_stopped": "Test has been stopped.",
        #
        "connection_err": "Server connection failed.",
        #
        "dload_green": "Measurements above 10 Mb/s",
        "dload_yellow": "Measurements between 2 and 10 Mb/s",
        "dload_red": "Measurements under 2 Mb/s",
        "upload_green": "Measurements above 2 Mb/s",
        "upload_yellow": "Measurements between 1 and 2 Mb/s",
        "upload_red": "Measurements under  1 Mb/s",
        "ping_green": "Measurements under 25 ms",
        "ping_yellow": "Measurements between 25 and 75 ms",
        "ping_red": "Measurements above 75 ms",
        "stats_table_help": "The table indicates <a href='help.html#43-what-is-a-percentile-and-a-median'>median</a> of measured values. You can find the meaning of individual colors on our <a href='help.html#36-how-do-i-know-if-the-test-result-is-good-or-bad'>help page</a>.",
        "service_end": "NetMetr was discontinued on April 30, 2022. Read more in the <a href='https://www.nic.cz/page/4305/sdruzeni-cznic-ukonci-projekt-netmetr/'>press release</a>."
    }
}

DEFAULT_LANG = "cs"

FEED_ALL_ATOM = None

FEED_ALL_RSS = None

DEFAULT_PAGINATION = 2

SUMMARY_MAX_LENGTH = 25

DEFAULT_DATE_FORMAT = "%A %d. %B %Y"

RELATIVE_URLS = True

THEME = "./theme"

PLUGIN_PATHS = ["./pelican-plugins"]

PLUGINS = [
    "sitemap",
    "minify",
    "assets",
    "extract_toc",
    "i18n_subsites"
]

SITEMAP = {
    "format": "xml",
    "priorities": {
        "articles": 0.6,
        "indexes": 0.3,
        "pages": 0.9
    },
    "changefreqs": {
        "articles": "daily",
        "indexes": "daily",
        "pages": "weekly"
    }
}

SASS_DEBUG_INFO = True
SASS_STYLE = "compact"

ARTICLE_URL = "novinky/{category}/{date:%Y}-{date:%m}-{date:%d}-{slug}.html"
ARTICLE_SAVE_AS = ARTICLE_URL

PAGE_URL = "{slug}.html"
PAGE_SAVE_AS = PAGE_URL

CATEGORY_URL = "novinky/{slug}/index.html"
CATEGORY_SAVE_AS = CATEGORY_URL

CATEGORIES_SAVE_AS = "novinky/index.html"

DIRECT_TEMPLATES = ()

MARKDOWN = {'extensions': ['toc']}

ASSET_SOURCE_PATHS = ['static']

PIWIK_URL = "piwik.nic.cz"
PIWIK_SITE_ID = "34"
SENTRY_DSN = "https://6ea46b4160514f6185d1255369a6e43f@sentry.labs.nic.cz/4"


class AssetsUrl(Filter):
    """Webassets filter. Replaces __SITEURL__ with website's root url."""
    name = "siteurl"

    def output(self, _in, out, **kwargs):
        out.write(_in.read())

    def input(self, _in, out, **kwargs):
        out.write(_in.read().replace("__SITEURL__", ROOTURL))


register_filter(AssetsUrl)


def siteurl(text):
    """
    Jinja2 filter.
    Replaces {{SITEURL}} with website's root url.
    Usage: {{ page.foo | siteurl }}
    """
    return text.replace("{{SITEURL}}", ROOTURL)


def lookup_lang_name(lang_code):
    return languages_lookup[lang_code]


def lookup_lang_slug(page, lang):
    return getattr(page, "slug_" + lang, None)


def dont_break(text, lang):
    """
    Jinja2 filter.
    Adds a non-breakable space after short prepositions.
    Usage: {{ page.foo | dont_break }}
    """
    if(lang == "cs"):
        SHORTWORDS = r"([aA]|[Vv]|[Oo]|[Nn]a|[Dd]o|[Ii])\s"
    else:
        SHORTWORDS = r"([Tt]he|[Oo]f|[Tt]o|[Ii]t|[Ii]s|[Bb]y|[Aa]|[Ii]n|[Aa]n)\s"
    return re.sub(SHORTWORDS, r"\1&nbsp;", text)


JINJA_FILTERS = {
    "lookup_lang_name": lookup_lang_name,
    "lookup_lang_slug": lookup_lang_slug,
    "dont_break": dont_break,
    "siteurl": siteurl
}
