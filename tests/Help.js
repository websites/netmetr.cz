module.exports = {
    "Help index builds": function(browser) {
        browser
            .url(browser.launchUrl + "napoveda.html")
            .waitForElementVisible("body", "Document body loads")
            .click("div.toc ul:first-child li:first-child")
            .waitForElementVisible("div.toc ul:first-child li:first-child li:first-child a span", "Link to section 1.1 is present")
            .end();
    }
};
