module.exports = {
    "Heatmap layer loads": function(browser) {
        browser
            .url(browser.launchUrl + "mapa.html")
            .waitForElementVisible("body", "Document body loads")
            .waitForElementNotVisible(".loader-overlay", "Loader fades away")
            .waitForElementVisible(".leaflet-tile-loaded[src*='heatmap']:first-child", "Loaded heatmap tile is present")
            .end();
    },
    "Points layer loads": function(browser) {
        browser
            .url(browser.launchUrl + "mapa.html")
            .waitForElementVisible("body", "Document body loads")
            .waitForElementNotVisible(".loader-overlay", "Loader fades away")
            .elements("css selector", ".leaflet-control-layers-overlays label:last-child input:not(:checked)", results => {
                if (results.value.length > 0) {
                    browser.click(".leaflet-control-layers-overlays label:last-child input:not(:checked)")
                }
            });
        browser
            .waitForElementVisible(".leaflet-tile-loaded[src*='points']:first-child", "Loaded points tile is present")
            .end();
    },
};
