var request = require("request");

module.exports = {
    "OpenData ZIP downloads": function(browser) {
        var zipUrl, host, path;
        browser
            .url(browser.launchUrl + "open-data.html")
            .waitForElementVisible("body", "Document body loads")
            .waitForElementVisible("#month-select option:first-child", "At least one month is available in selectbox")
            .click("#month-select option:last-child")
            .click("#btn-download-month")
            .assert.attributeContains("#btn-download-month", "data-url", ".zip", "Download URL is built")
            .getAttribute("#btn-download-month", "data-url", function(result) {
                zipUrl = result.value;
                request(zipUrl, {
                    method: "HEAD"
                }, function(err, res) {
                    browser.assert.equal(res.headers["content-type"], "application/zip", "File has correct content-type");
                    browser.assert.equal(res.headers["content-disposition"], zipUrl.replace(/.*\//, "attachment; filename="), "File has correct filename");
                });
            });
        browser.end()
    }
};
