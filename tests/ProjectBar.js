module.exports = {
    "Project bar loads": function(browser) {
        browser
            .url(browser.launchUrl + "napoveda.html")
            .resizeWindow(1000, 800)
            .waitForElementVisible("body", "Document body loads")
            .waitForElementVisible("#tb-projects-bar", "Project bar is present")
            .end();
    }
};
