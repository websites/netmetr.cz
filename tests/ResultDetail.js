module.exports = {
    "Speed chart draws": function(browser) {
        browser
            .url(browser.launchUrl + "statistiky.html")
            .waitForElementVisible("body", "Stats page loads")
            .waitForElementNotVisible(".loader-overlay", "Loader fades away")
            .click("#results tr:first-child td.datetime a")
            .waitForElementVisible("body", "Test detail loads after click")
            .waitForElementNotVisible(".loader-overlay", "Loader fades away")
            .waitForElementVisible("#placeholder-dl canvas.flot-overlay", "Download chart is present")
            .end();
    }
};
