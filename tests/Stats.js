module.exports = {
    "Devices table loads": function(browser) {
        browser
            .url(browser.launchUrl + "statistiky.html")
            .waitForElementVisible("body", "Document body loads")
            .waitForElementNotVisible(".loader-overlay", "Loader fades sway")
            .waitForElementVisible("#devices tr:first-child td.sorting_1", "At least one result is present in the Devices table")
            .end();
    },
    "Operators table loads": function(browser) {
        browser
            .url(browser.launchUrl + "statistiky.html")
            .waitForElementVisible("body", "Document body loads")
            .waitForElementNotVisible(".loader-overlay", "Loader fades sway")
            .waitForElementVisible("#operators tr:first-child td.sorting_1", "At least one result is present in the Operators table")
            .end();
    },
    "Stats table loads": function(browser) {
        browser
            .url(browser.launchUrl + "statistiky.html")
            .waitForElementVisible("body", "Document body loads")
            .waitForElementNotVisible(".loader-overlay", "Loader fades sway")
            .waitForElementVisible("#operators tr:first-child td.sorting_1", "At least one result is present in the Results table")
            .end();
    }
};
