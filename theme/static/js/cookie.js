function createCookieIfNotPresent(successCallback, errorCallback) {
    // if cookie isn't present (or it's uuid not valid), get uuid from RMBTControlServer
    // and save it to a new cookie
    var uuidCookie = Cookies.get(window.cookieName);
    if (/^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$/.test(uuidCookie)) {
        if(successCallback) successCallback();
    } else {
        $.ajax({
            "url": controlUrl + "settings",
            "type": "post",
            "dataType": "json",
            "contentType": "application/json",
            "data": JSON.stringify({
                "language": getPageLanguage(),
                "type": "DESKTOP",
                "name": "RMBT",
                "terms_and_conditions_accepted_version": 1,
                "uuid": null
            }),
            success: function(data) {
                var uuid = data.settings[0].uuid;
                Cookies.set(window.cookieName, uuid, { expires: 365 });
                if(successCallback) successCallback();
            },
            error: function() {
                if(errorCallback) {
                    errorCallback();
                } else {
                    console.error(i18n_general.connectionErr);
                }
            }
        });
    }
}
