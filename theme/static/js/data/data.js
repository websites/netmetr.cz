$(document).ready(function() {
    var monthDownloadBaseUrl = statisticUrl + "export/NetMetr-opendata";

    getMonthList(true);

    // bind download link to button:
    $("button#btn-download-month").click(function(event) {
        event.preventDefault();
        var downloadUrl = monthDownloadBaseUrl + "-" + monthSelect.val() + ".zip";
        $("button#btn-download-month").attr("data-url", downloadUrl);
        window.location = downloadUrl;
    });
});
