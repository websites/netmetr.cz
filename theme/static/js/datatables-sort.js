jQuery.fn.dataTableExt.oSort["numeric_ignore_nan-asc"]  = function(x,y) {
    if (isNaN(x) && isNaN(y)) return ((x < y) ? 1 : ((x > y) ?  -1 : 0));
 
    if (isNaN(x)) return -1;
    if (isNaN(y)) return 1;
 
    x = parseFloat( x );
    y = parseFloat( y );
    return ((x < y) ? -1 : ((x > y) ?  1 : 0));
};
  
jQuery.fn.dataTableExt.oSort["numeric_ignore_nan-desc"] = function(x,y) {
    if (isNaN(x) && isNaN(y)) return ((x < y) ? 1 : ((x > y) ?  -1 : 0));
 
    if (isNaN(x)) return 1;
    if (isNaN(y)) return -1;
 
    x = parseFloat( x );
    y = parseFloat( y );
    return ((x < y) ?  1 : ((x > y) ? -1 : 0));
};