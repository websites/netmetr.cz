UTCToLocal = function(date) {
  /* converts UTC date string to local timezone and date string format */
  var UTCdate = new Date(date.replace(/-/g, "/") + " UTC");
  return (
    UTCdate.toLocaleDateString(document.documentElement.lang) +
    " " +
    UTCdate.toLocaleTimeString(document.documentElement.lang)
      .replace(/:\d{2}(\s|$)/, "") // removes seconds
      .replace(/([AP]M)/, "&nbsp;$1") // adds a non-breakable space before AM/PM
      .replace(/GMT.*$/, "")
  ); // removes timezone
};
