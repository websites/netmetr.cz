$(document).ready(function() {
    showLoaderOverlay();
    var uuid = window.location.search.substring(1);

    $("li.active.index-link").removeClass("active");

    $("#placeholder-dl").height($("#placeholder-dl").width() / 1.8);
    $("#placeholder-ul").height($("#placeholder-ul").width() / 1.8);

    if (!isValidUUID(uuid)) {
        hideLoaderOverlay();
        alert("Neplatné ID testu.");
        return false;
    } else if (/^O/.test(uuid)) {
        loadStatsTable(uuid);
        loadQoSTable(uuid);
    } else {
        getOpenUUIDAndLoadStats(uuid);
    }
});
