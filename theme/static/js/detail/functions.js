isValidUUID = function(uuid) {
    return /^O?[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$/.test(uuid);
};

function getOpenUUIDAndLoadStats(uuid) {
    var type = "post";
    var data = JSON.stringify({
        "test_uuid": uuid
    });
    var url = controlUrl + "testresultdetail";

    $.ajax({
        type: type,
        url: url,
        data: data,
        dataType: "json",
        contentType: "application/json",
        success: function(r) {
            if (r.testresultdetail && r.testresultdetail.length > 0) {
                r.testresultdetail.forEach(function(item) {
                    if (item.title === "Open Test ID") {
                        loadStatsTable(item.value);
                        loadQoSTable(item.value);
                    }
                });
            }
        }
    });
}

function loadStatsTable(uuid) {
    var type = "get";
    var url = controlUrl + "opentests/" + uuid;

    $.ajax({
        type: type,
        url: url,
        dataType: "json",
        contentType: "application/json",
        success: function(r) {
            if (r.download_kbit) {
                $(".dtl.download_kbit")
                    .append("<span class='classification' data-classification='" +
                        r.download_classification + "'></span>" + (r.download_kbit / 1000).toFixed(
                            3) + " Mb/s");
            } else {
                $(".dtl.download_kbit").parent().addClass("hidden");
            }

            if (r.upload_kbit) {
                $(".dtl.upload_kbit")
                    .append("<span class='classification' data-classification='" +
                        r.upload_classification + "'></span>" + (r.upload_kbit / 1000).toFixed(
                            3) + " Mb/s");
            } else {
                $(".dtl.upload_kbit").parent().addClass("hidden");
            }

            if (r.ping_ms) {
                $(".dtl.ping_ms")
                    .append("<span class='classification' data-classification='" +
                        r.ping_classification + "'></span>" + r.ping_ms.toFixed(2) + " ms");
            } else {
                $(".dtl.ping_ms").parent().addClass("hidden");
            }

            if (r.signal_strength) {
                $(".dtl.signal_strength").text(r.signal_strength + " dBm");
            } else {
                $(".dtl.signal_strength").parent().addClass("hidden");
            }

            if (r.long && r.lat) {
                $(".dtl.time")
                    .append(UTCToLocal(r.time) + " <a title='Mapa oblasti' href='mapa.html?lon=" +
                        r.long + '&amp;lat=' + r.lat + "' class='map-link'></a>");
            } else {
                $(".dtl.time").append(UTCToLocal(r.time));
            }

            $(".dtl.model_native").text(r.model_native + " (" + r.platform + ")");

            if (r.provider_name) {
                $(".dtl.network_name").text(r.provider_name);
            } else if (r.network_name) {
                $(".dtl.network_name").text(r.network_name);
            } else if (r.public_ip_as_name) {
                $(".dtl.network_name").text(r.public_ip_as_name);
            } else {
                $(".dtl.network_name").parent().addClass("hidden");
            }

            if (r.network_type) {
                $(".dtl.network_type").text(r.network_type);
            } else {
                $(".dtl.network_type").parent().addClass("hidden");
            }

            if (r.cat_technology) {
                $(".dtl.cat_technology").text(r.cat_technology);
            } else {
                $(".dtl.cat_technology").parent().addClass("hidden");
            }

            if (r.speed_curve) {
                if (r.speed_curve.download.length > 0) {
                    drawSingleSpeedCurve("#placeholder-dl", r.speed_curve.download, "download");
                }
                
                if (r.speed_curve.upload.length > 0) {
                    drawSingleSpeedCurve("#placeholder-ul", r.speed_curve.upload, "upload");
                }
                
                if (r.speed_curve.download.length == 0 && r.speed_curve.upload.length == 0) {
                    document.querySelector("section.charts").classList.add("hidden");
                }
            }

            $(".dtl.connection").text(r.connection);
            $(".dtl.client_version").text(r.client_version);

            hideLoaderOverlay();
        },
        error: function() {
            hideLoaderOverlay();
        }
    });
}


function loadQoSTable(uuid) {
    if (!isValidUUID(uuid)) {
        hideLoaderOverlay();
        return false;
    }

    $.ajax({
        type: "get",
        url: controlUrl + "qos/" + uuid,
        dataType: "json",
        contentType: "application/json",
        data: {
            language: getPageLanguage()
        },
        success: function(r) {
            if (r.testresultdetail) {
                for (var i in r.testresultdetail_testdesc) {
                    var type = r.testresultdetail_testdesc[i].test_type.toLowerCase();
                    var qosTestContainer = $("<section />").addClass("qos-container");
                    var header = $("<header />").addClass("qos-header");
                    var heading = $("<h2 />").addClass("qos-name").text(
                        "→ " + r.testresultdetail_testdesc[i].name);
                    var description = $("<p />").html(r.testresultdetail_testdesc[i].desc);
                    description.append(
                        "<br><button class='btn-open-detail'>→ "+ i18n_detail.display_tests +"</button>"
                    );
                    header.append(heading)
                        .append(description);
                    qosTestContainer.append(header);

                    for (var i in r.testresultdetail) {
                        if (r.testresultdetail[i].test_type == type) {
                            var resultLine = $("<article>").addClass("qos-result");
                            var resultHeader = $("<header>").addClass("qos-basic");
                            var resultDetail = $("<footer>").addClass("qos-detail");

                            if (r.testresultdetail[i].failure_count == 0) {
                                var color = "3"; // green
                            } else {
                                var color = "1"; // red
                            }
                            resultHeader.append(
                                "<span class='classification' data-classification='" + color +
                                "' />");

                            var summary = $("<p />").html(r.testresultdetail[i].test_summary.replace(
                                /\n/g, "<br />"));
                            resultHeader.append(summary);

                            var desc = $("<p />").html(r.testresultdetail[i].test_desc.replace(
                                /\n/g, "<br />"));
                            resultDetail.append(desc);

                            resultLine.append(resultHeader).append(resultDetail);
                            qosTestContainer.append(resultLine);
                        }
                    }

                    qosTestContainer.appendTo($("section.qos"));
                }

                $(".qos-result").hide();
                $(".qos-header p").hide();

                $(".qos-name").click(function(event) {
                    event.preventDefault();

                    var header = $(this);

                    if (!header.hasClass("opened")) {
                        header.addClass("opened");
                        header.text(function(i, current) {
                            return current.replace("→", "↓");
                        });
                    } else {
                        header.removeClass("opened");
                        header.text(function(i, current) {
                            return current.replace("↓", "→");
                        });
                        header.parent().siblings(".qos-result").slideUp();
                        header.siblings("p").children(".btn-open-detail").text(
                            "→ " + i18n_detail.display_tests);
                    }

                    header.siblings("p").slideToggle(200);
                });

                $(".btn-open-detail").click(function(event) {
                    event.preventDefault();

                    var button = $(this);

                    if (!button.hasClass("opened")) {
                        button.addClass("opened");
                        button.text("↓ " + i18n_detail.hide_tests);
                    } else {
                        button.removeClass("opened");
                        button.text("→ " + i18n_detail.display_tests);
                    }

                    button.parent().parent().siblings(".qos-result").slideToggle();
                });

                $(".qos-container").each(function() {
                    var container = $(this);

                    var redCount = container.find(".classification[data-classification='1']").length;
                    var greenCount = container.find(".classification[data-classification='3']").length;

                    if (redCount > 0) {
                        var color = "1"; // red
                    } else if (greenCount > 0) {
                        var color = "3"; // green
                    } else {
                        var color = "0" // gray
                        container.find(".btn-open-detail").addClass("hidden"); // hide button
                    }

                    container.children(".qos-header").prepend(
                        "<span class='classification' data-classification='" + color +
                        "' />");
                });

                $(".qos").removeClass("hidden");
            }
        },
        error: function() {
            hideLoaderOverlay();
        }
    });
}


function drawSingleSpeedCurve(target, data, phase) {
    //smoothing-factor for exponential smoothing
    var alpha = 0.7;
    var conAlpha = 1 - alpha;

    //get data points from json-response from the controlserver
    var previousSpeed = data[0].bytes_total / data[0].time_elapsed;
    var previousData = 0;
    var previousTime = 0;
    var dDownload = [];
    for (var i = 0; i < data.length; ++i) {
        var dataDifference = data[i].bytes_total - previousData;
        var timeDifference = data[i].time_elapsed - previousTime;

        //only one point every 250 ms or data is way too late (120 sek)
        if (timeDifference < 175 || data[i].time_elapsed > 120000) {
            continue;
        }

        var speed = dataDifference / timeDifference;
        speed = speed * alpha + previousSpeed * conAlpha;
        previousSpeed = speed;
        previousData = data[i].bytes_total;
        previousTime = data[i].time_elapsed;

        speed = Math.log(speed);
        dDownload.push([data[i].time_elapsed, speed]); //byte : 128 = kilobit; mbit/sec = kbit/ms


        var placeholder = $(target);

        //draw the plot
        $.plot(placeholder, [dDownload], {
            shadowSize: 0,
            lines: {
                show: true,
                fill: true,
                lineWidth: 2
            },
            xaxis: {
                show: true,
                tickFormatter: function(v) {
                    return (v / 1000).toFixed(0) + "&nbsp;s";
                },
                tickColor: "#eee",
                tickSize: 1000
            },
            yaxis: {
                show: true,
                tickFormatter: function(v) {
                    //inverse function to log from above
                    v = ((Math.pow(Math.E, v) / 125));
                    //format <1mbps with 2 decimal points
                    if (v > 4) {
                        return v.toFixed(0); // + "&nbsp;" + 'Mb/s';
                    } else {
                        return v.toFixed(1); // + "&nbsp;" + 'Mb/s';
                    }
                },
                tickColor: "#f5f5f5",
                ticks: [
                    [0, "0"], 2, 4.828313, 7.1308988, 9.4334839
                ], //ln(125), ln(1250), ln(12500)
                // ticks: [0, 10, 50, 100],
                min: 0
                    // max: 100
            },
            grid:  {
                borderWidth: 1,
                borderColor: "#ddd"
            },
            colors: [(phase === "download") ? "#1d3e89" : "#3db7e4"]
        });
    }
}
