var revisedMonthsUrl = statisticUrl + "months";
var monthSelect = $("#month-select");
var options = [];
var monthNames = {}
monthNames["cs"] = ["leden", "únor", "březen", "duben", "květen", "červen", "červenec", "srpen", "září", "říjen", "listopad", "prosinec"];
monthNames["en"] = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

var getMonthList = function(checked) {
    // populate year select with months:
    $.ajax({
        "url": revisedMonthsUrl,
        "type": "post",
        "dataType": "json",
        "contentType": "application/json",
        "data": JSON.stringify({
            checked: checked.toString(),
            language: getPageLanguage()
        }),
        success: function(jsonData) {
            if (Object.keys(jsonData.revised_months[0]).length < 1) {
                alert("Žádné schválené měsíční výsledky.");
                hideLoaderOverlay();
                $("#btn-approve").classList.add("hidden")
                return;
            } else {
                $.each(jsonData.revised_months[0], function(year, months) {
                    months.forEach(function(month) {
                        options.push("<option value='" + year + "-" + ("0" + month).slice(-2) +
                        "'>" + monthNames[getPageLanguage()][month - 1] + " " + year + "</option>");
                    });
                });

                // sort from oldest to newest:
                options.sort();

                // insert into DOM:
                monthSelect.append(options);

                // pre-select most recent month and year:
                $("#month-select option:last").attr("selected", "selected");
            }
        },
        error: function() {
            console.error(i18n_general.connectionErr);
            hideLoaderOverlay();
        }
    });
};
