$(document).ready(function() {
    var element = $('.toc').detach();
    $('.sidebar').append(element);

    $(".sidebar ul li ul").hide();
    $(".sidebar ul li a").each(function() {
        var me = $(this);
        me.html(me.text()
            .replace(/(^[0-9]+\.[0-9]*)/, "<strong>$1</strong><span>")
            .replace(/$/, "</span>"));
    });

    var anchor = window.location.hash.replace('#', '');
    if (anchor) {
        document.getElementById(anchor).scrollIntoView();
    }

    $(".pagetext.help :header").each(function() {
        var me = $(this);
        me.append("&nbsp;<a href='#" + me.attr("id") +
            "' class='anchor'>#</a>");

    });

    $(".toc > ul > li > a").click(function(event) {
        event.preventDefault();
        $(this).siblings("ul").slideToggle();
    });
});
