/* global nicWidgets */
document.addEventListener("DOMContentLoaded", function() {
  var lang =
    document.documentElement.lang ||
    window.navigator.userLanguage ||
    window.navigator.language;

  if (typeof window.nicWidgets === "object") {
    if (/^.*cs.*$/.test(lang)) {
      window.nicWidgetsBar = new nicWidgets.topBar("CS");
    } else if (/^.*en.*$/.test(lang)) {
      window.nicWidgetsBar = new nicWidgets.topBar("EN");
    } else {
      window.nicWidgetsBar = new nicWidgets.topBar("EN"); // fall back to default
    }
  }
});
