getPageLanguage = function() {
    var lang = document.querySelector("html").lang;
    return lang === "cs" ? "cs" : "en";
};

dataTablesLang = function(replacements) {
    var lang;
    if(!replacements){
        replacements = {
            cs: {},
            en: {}
        }
    }
    if(getPageLanguage() === "cs") {
        lang = {
            "sLengthMenu": replacements.cs.sLengthMenu || "Zobrazit _MENU_ záznamů",
            "sZeroRecords": replacements.cs.sZeroRecords || "Žádné záznamy nebyly nalezeny",
            "sInfo": replacements.cs.sInfo || "Zobrazeno _START_ až _END_ z celkem _TOTAL_ záznamů",
            "sInfoEmpty": replacements.cs.sInfoEmpty || "Zobrazeno 0 záznamů",
            "sInfoFiltered": replacements.cs.sInfoFiltered || "(filtrováno z celkem _MAX_ záznamů)",
            "sInfoPostFix": replacements.cs.sInfoPostFix || "",
            "sSearch": replacements.cs.sSearch || "Filtrovat&nbsp;",
            "sUrl": replacements.cs.sUrl || "",
            "oPaginate": replacements.cs.oPaginate || {
                "sFirst": replacements.cs.sFirst || "První",
                "sPrevious": replacements.cs.sPrevious || "← Předchozí",
                "sNext": replacements.cs.sNext || "Další →",
                "sLast": replacements.cs.sLast || "Poslední"
            },
            "decimal": replacements.cs.decimal || ","
        }
    } else {
        lang = {
            "sLengthMenu": replacements.en.sLengthMenu || "Display _MENU_ records",
            "sZeroRecords": replacements.en.sZeroRecords || "No records found.",
            "sInfo": replacements.en.sInfo || "_START_ to _END_ of _TOTAL_ total records",
            "sInfoEmpty": replacements.en.sInfoEmpty || "Displayed 0 records",
            "sInfoFiltered": replacements.en.sInfoFiltered || "(filtered from _MAX_ total results)",
            "sInfoPostFix": replacements.en.sInfoPostFix || "",
            "sSearch": replacements.en.sSearch || "Filter:&nbsp;",
            "sUrl": replacements.en.sUrl || "",
            "oPaginate": replacements.en.oPaginate || {
                "sFirst": replacements.en.sFirst || "First",
                "sPrevious": replacements.en.sPrevious || "← Previous",
                "sNext": replacements.en.sNext || "Next →",
                "sLast": replacements.en.sLast || "Last"
            },
            "decimal": replacements.en.decimal || "."
        }
    }
    return lang;
}
