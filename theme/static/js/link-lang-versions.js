document.addEventListener("DOMContentLoaded", function() {
    var otherslugEl = document.querySelector("[data-otherslug]");
    var linkEl = document.querySelector(".nav-language a");
    if(otherslugEl && linkEl) {
        linkEl.href = otherslugEl.dataset.otherslug + window.location.search;
    }
});
