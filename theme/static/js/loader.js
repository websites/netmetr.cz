showLoaderOverlay = function() {
    if ($(".loader-overlay").length === 0) {
        var over = "<div class='loader-overlay'><div class='circle'></div></div>";
        $(over).appendTo("body");
    } else {
        $(".loader-overlay").fadeIn();
    }
};

hideLoaderOverlay = function() {
    $(".loader-overlay").fadeOut();
};
