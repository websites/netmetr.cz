$("select#select-percentile").change(function() {
    showPercentileSelect();
});

$("select#select-period").change(function() {
    showPeriodSelect();
});

$("select#select-technology").change(function() {
    showTechnologySelect();
});

$("select#select-operator").change(function() {
    showOperatorSelect();
});

$("select#select-type").change(function() {
    showMapTypeSelect();
    fulltype = $("#select-type option:selected").val(); // eg. "mobile/download"
    var type = fulltype.split("/")[0]; // just the "mobile" part
    map.closePopup(); // close any opened popups
    switch (type) {
        case "mobile":
            showTechnologySelect();
            hideOperatorSelect();
            showOperatorSelect(type);
            showColorGradient(fulltype);
            break;
        case  "wifi":
            hideTechnologySelect();
            hideOperatorSelect();
            // showOperatorSelect(type);
            showColorGradient(fulltype);
            break;
        case "browser":
            hideTechnologySelect();
            hideOperatorSelect();
            // showOperatorSelect(type);
            showColorGradient(fulltype);
            break;
        case "all":
            hideTechnologySelect();
            hideOperatorSelect();
            showColorGradient(fulltype);
            break;
    }
});
