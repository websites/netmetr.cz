// <select> for map type:
function showMapTypeSelect() {
    var mapTypeSelect = $("select#select-type");
    // if <select> is empty (first run):
    if (mapTypeSelect.children().length === 0) {
        // create <select> fields from infoJSON:
        var options = [],
            i, j;
        for (i = 0; i < infoJSON.mapfilter.mapTypes.length; i++) {
            options.push("<optgroup label='" + infoJSON.mapfilter.mapTypes[i].title +
                "'>");
            for (j = 0; j < infoJSON.mapfilter.mapTypes[i].options.length; j++) {
                options.push("<option value='" + infoJSON.mapfilter.mapTypes[i].options[j]
                    .map_options + "'>" +
                    infoJSON.mapfilter.mapTypes[i].title + " – " +
                    infoJSON.mapfilter.mapTypes[i].options[j].title + "</option>");
            }
            options.push("</optgroup>");
        }
        mapTypeSelect.append(options.join(""));
    } else {
        // get selected value & use it in tile URL:
        var mapType = mapTypeSelect.val();
        mapOptions = mapOptions.replace(/&map_options=\w+\/\w+/g, "");
        if (mapType !== "") {
            mapOptions += "&map_options=" + mapType;
        }
        updateLayers(mapOptions);
    }

    // show <select>:
    mapTypeSelect.removeClass("hidden");
}

// <select> for percentile:
function showPercentileSelect() {
    var percentileSelect = $("select#select-percentile");

    // if <select> is empty (first run):
    if (percentileSelect.children().length === 0) {
        // create <select> options from infoJSON:
        $.each(infoJSON.mapfilter.mapFilters.mobile[0].options, function(key, value) {
            percentileSelect.append("<option value='" + value.statistical_method + "'>" +
                value.title + "</option>");
            // select first option by default:
            percentileSelect.prop("selectedIndex", 0);
        });
    } else {
        // get selected value & use it in tile URL:
        var percentile = percentileSelect.val();
        mapOptions = mapOptions.replace(/&statistical_method=\d.\d/g, "");
        if (percentile !== "") {
            mapOptions += "&statistical_method=" + percentile;
        }
        updateLayers(mapOptions);
    }

    // show <select>:
    percentileSelect.removeClass("hidden");
}

// <select> for time period:
function showPeriodSelect() {
    var periodSelect = $("select#select-period");

    // if <select> is empty (first run):
    if (periodSelect.children().length === 0) {
        // create <select> options from infoJSON:
        $.each(infoJSON.mapfilter.mapFilters.mobile[2].options, function(key, value) {
            periodSelect.append("<option value='" + value.period + "'>" +
                value.title +
                "</option>");
            // select 6 months by default:
            periodSelect.prop("selectedIndex", 3);
        });
    } else {
        // get selected value & use it in tile URL:
        var period = periodSelect.val();
        mapOptions = mapOptions.replace(/&period=\d+/g, "");
        if (period !== "") {
            mapOptions += '&period=' + period;
        }
        updateLayers(mapOptions);
    }

    // show <select>:
    periodSelect.removeClass("hidden");
}

// <select> for operator:
function showOperatorSelect(type) {
    if (!type) {
        type = DEFAULT_TYPE.split("/")[0];
    }

    var operatorSelect = $("select#select-operator");

    // if <select> is empty (first run):
    if (operatorSelect.children().length === 0) {
        // create <select> options from infoJSON:
        $.each(eval("infoJSON.mapfilter.mapFilters." + type + "[1].options"),
            function(key, value) {
                operatorSelect.append("<option value='" + value.operator + "'>" + value.title +
                    "</option>");
                // select first option (all networks) by default:
                operatorSelect.prop("selectedIndex", 0);
            });
    } else {
        // get selected value & use it in tile URL:
        var operator = operatorSelect.val();
        mapOptions = mapOptions.replace(/&operator=\d+/g, "");
        if (operator !== "") {
            mapOptions += "&operator=" + operator;
        }
        updateLayers(mapOptions);
    }
    // show <select>:
    operatorSelect.removeClass("hidden");
}

function hideOperatorSelect() {
    $("select#select-operator").addClass("hidden");
    $("select#select-operator").empty();
    mapOptions = mapOptions.replace(/&operator=\d+/g, "");

    updateLayers(mapOptions);
}


// <select> for technology (2G/3G/4G/5G):
function showTechnologySelect() {
    var technologySelect = $("select#select-technology");

    // if <select> is empty (first run):
    if (technologySelect.children().length === 0) {
        // create <select> options from infoJSON:
        $.each(infoJSON.mapfilter.mapFilters.mobile[3].options, function(key, value) {
            technologySelect.append("<option value='" + value.technology + "'>" +
                value.title +
                "</option>");
            // select first option (all networks) by default:
            technologySelect.prop("selectedIndex", 0);
        });
    } else {
        // get selected value & use it in tile URL:
        var technology = technologySelect.val();
        mapOptions = mapOptions.replace(/&technology=\d+/g, "");
        if (technology !== "") {
            mapOptions += "&technology=" + technology;
        }
        updateLayers(mapOptions);
    }

    // show <select>:
    technologySelect.removeClass("hidden");
}

function hideTechnologySelect() {
    $("select#select-technology").addClass("hidden");
    mapOptions = mapOptions.replace(/&technology=\d+/g, "");
    updateLayers(mapOptions);
}

function showColorGradient(type) {
    if (!type) {
        type = DEFAULT_TYPE;
    }
    $.each(infoJSON.mapfilter.mapTypes, function(i, v) {
        $.each(v.options, function(j, w) {
            // find corresponding json section
            if (w.map_options == type) {
                // make css gradient:
                var gradient = "linear-gradient(90deg, ";
                for (var i = 0; i < w.heatmap_colors.length; i = i + 1) {
                    gradient = gradient + w.heatmap_colors[i] + ", ";
                }
                // remove comma and space after the last color:
                gradient = gradient.substr(0, gradient.length - 2);
                gradient = gradient + ")";
                // apply gradient to table
                $(".map-gradient").css("background", gradient);
                // remove all leftovers from previuos map type's legend
                $(".map-gradient tr").empty();
                // fill table cells with numbers from infojson:
                for (i = 0; i < w.heatmap_captions.length; i = i + 1) {
                    $('<td></td>').text(w.heatmap_captions[i]).appendTo($(
                        '.map-gradient tr'));
                }
                // append unit to last number:
                $(".map-gradient tr td:last").append("&nbsp;" + w.unit);
                // use summary as gradient tooltip:
                $(".map-gradient").prop("title", w.summary);
                // return;
            }
        });
    });
}

function updateLayers(mapOptions) {
    heatmapLayer.setUrl(mapServer + "heatmap" + mapPath + mapOptions);
    pointsLayer.setUrl(mapServer + "points" + mapPath + mapOptions);
}

function loadMap(showNetmetrLayers) {
    hideLoaderOverlay();

    var osmLayer = L.tileLayer(
        "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
            attribution: "© OpenStreetMap contributors"
        });

    var cartodbLightLayer = L.tileLayer(
        "https://cartodb-basemaps-c.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png", {
            attribution: "© cartoDB, © OpenStreetMap contributors"
        });

    if (showNetmetrLayers) {
      heatmapLayer = L.tileLayer(mapServer + "heatmap" + mapPath + mapOptions, {
          attribution: "© CZ.NIC"
      });

      pointsLayer = L.tileLayer(mapServer + "points" + mapPath + mapOptions, {
          attribution: "© CZ.NIC"
      });
    }

    map = new L.Map('map', {
        center: new L.LatLng(49.8108534, 15.6122417),
        zoom: 8,
        layers: [cartodbLightLayer, showNetmetrLayers && heatmapLayer],
        maxZoom: MAP_MAX_ZOOM,
        scrollWheelZoom: true
    });

    // create layer control widget:
    var mapLayers = {};

    mapLayers[layerNames.cartodb] = cartodbLightLayer;
    mapLayers[layerNames.osm] = osmLayer;

    var resultLayers = {};

    if (showNetmetrLayers) {
      resultLayers[layerNames.heat] = heatmapLayer;
      resultLayers[layerNames.points] = pointsLayer;
    }

    map.addControl(new L.Control.Layers(mapLayers, resultLayers, {
        position: "bottomleft",
        collapsed: false
    }));

    // create listener for measurement popups:
    map.on("click", function(e) {
        showPopup(e);
    });

    // create listener for showing/hiding layers based on map zoom:
    map.on("zoomend", function() {
        if (map.getZoom() > 14 && map.hasLayer(pointsLayer) == false) {
            map.addLayer(pointsLayer);
        }
    });
}

function showPopup(e) {
    if (map.hasLayer(pointsLayer)) { // if pointsLayer is visible
        // get current map options (or use defaults):
        var percentile = $("#select-percentile option:selected").val() || DEFAULT_PERCENTILE;
        var period = $("#select-period option:selected").val() || DEFAULT_PERIOD;
        var type = $("#select-type option:selected").val() || DEFAULT_TYPE;
        var provider = $("#select-operator option:selected").val() || null;
        var zoom = map.getZoom();
        // create json:
        var json_data = {
            language: getPageLanguage(),
            coords: {
                lat: e.latlng.lat,
                lon: e.latlng.lng,
                z: zoom,
                size: 50-zoom
            },
            filter: {
                statistical_method: percentile,
                period: period,
            },
            options: {
                map_options: type
            }
        };
        if (provider !== null && provider !== "undefined") {
            json_data["filter"]["provider"] = provider.toString();
        }
        // get response:
        $.ajax({
            url: mapServer + "markers",
            type: "post",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(json_data),
            success: function(data) {
                try {
                    // if server returned at least 1 measurement:
                    if (data.measurements.length > 0) {
                        // fill popup:
                        var popupContent = "<h3>" + i18n_map.measurements + "</h3><section class='measurement-data'>";
                        for (i = 0; i < data.measurements.length; i++) {
                            popupContent = popupContent +
                                "<article class='measurement'><h4><a href='detail.html?" +
                                data.measurements[i].open_test_uuid + "'>" +
                                data.measurements[i].time_string +
                                "</a></h4><h5>" + i18n_map.results + "</h5><table class='measurements'>";
                            for (j = 0; j < data.measurements[i].measurement.length; j++) {
                                popupContent = popupContent + "<tr><td>" +
                                    data.measurements[i].measurement[j].title +
                                    "</td><td class='classification'>" +
                                    "<span class='classification' data-classification='" +
                                    data.measurements[i].measurement[j].classification +
                                    "'></span></td><td>" +
                                    data.measurements[i].measurement[j].value + "</td></tr>";
                            }
                            popupContent = popupContent +
                                "</table><h5>Síť</h5><table class='network'>";
                            for (j = 0; j < data.measurements[i].net.length; j++) {
                                popupContent = popupContent + "<tr><td>" + data.measurements[i].net[j].title +
                                    "</td><td>" + data.measurements[i].net[j].value + "</td></tr>";
                            }
                            popupContent = popupContent + "</table></article>";
                        }
                        popupContent = popupContent + "</section>";
                        // …and display it on map:
                        L.popup()
                            .setLatLng([e.latlng.lat, e.latlng.lng])
                            .setContent(popupContent)
                            .openOn(map);
                    } else {
                        var err = new Error("Map click: server returned an empty measurement array.")
                        throw err;
                    }
                }
                catch(err){
                    Raven.captureMessage(err, {
                        level: "info",
                        extra: json_data,
                        tags: ["map", "RMBTMapServer"]
                    });
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                    Raven.captureMessage(thrownError, {
                        level: "info",
                        extra: json_data,
                        tags: ["map", "evilPlace", "RMBTMapServer"]
                    });
            }
        });
    } else {
        return;
    }
}

function getURLParameter(param) {
    var svalue = location.search.match(new RegExp("[?&]" + param + "=([^&]*)(&?)",
        "i"));
    return svalue ? svalue[1] : svalue;
}
