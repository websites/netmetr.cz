var mapOptions = "&point_diameter=8",
    mapPath = "?path={z}/{x}/{y}",
    DEFAULT_TYPE = "mobile/download",
    DEFAULT_PERIOD = 180,
    DEFAULT_PERCENTILE = 0.8,
    // MAP_DEFAULT_ZOOM = 7,
    MAP_MIN_ZOOM = 7,
    MAP_MAX_ZOOM = 18;

// L.Icon.Default.imagePath = (function() {
//     return "theme/images/pixel.png?";
// }());

$(document).ready(function() {
    showLoaderOverlay();

    //get info json from RMBT server:
    $.ajax({
        type: "post",
        url: mapServer + "info",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify({
            language: getPageLanguage()
        }),
        success: function(response) {
            infoJSON = response;

            // load layers into map:
            loadMap(true);

            // if we got coords as url paramaters:
            lon = getURLParameter("lon");
            lat = getURLParameter("lat");
            if (lon && lat) {
                // pan to desired location:
                setTimeout(function(){ map.panTo(new L.LatLng(lat, lon))}, 300);
                // and zoom in:
                map.setZoom(15);
            } else { // if not, try to geolocate user and pan to their location:
                map.locate({
                    setView: true,
                    enableHighAccuracy: true
                });
            }

            // populate selects:
            showMapTypeSelect();
            showPercentileSelect();
            showPeriodSelect();
            showOperatorSelect();
            showTechnologySelect();
            showColorGradient();
        },
        error: function() {
            console.error(i18n_general.connectionErr);
            hideLoaderOverlay();
            loadMap(false);
        }
    });
});
