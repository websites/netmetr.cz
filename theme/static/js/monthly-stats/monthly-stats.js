$(document).ready(function() {
    $(".index-link").removeClass("active");

    $(".field-wrapper.stats-duration").hide();
    $(".field-wrapper.stats-duration select").val(30);

    var monthNames = {};
    monthNames["cs"] = ["leden", "únor", "březen", "duben", "květen", "červen", "červenec", "srpen", "září", "říjen", "listopad", "prosinec"];
    monthNames["en"] = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    var loadMultipleMonths = function(count) {
        showLoaderOverlay();
        var months = [];
        for (i = 1; i < count + 1; i++) {
            var month = new Date(validMonths[i - 1] + "-01");
            months.push(month);
        }

        var i = 1;
        months.forEach(function(month) {
            var tableName = "month" + i + "-table";
            $(".stats-wrapper").append("<h2 class='mono'>" + monthNames[getPageLanguage()][month.getMonth()] + " " + month.getFullYear() + "</h2>");
            $(".stats-wrapper").append("<div class='table-wrapper'><table id='" + tableName + "'><thead><tr><th>Název</th>" +
                "<th>Stahování</th><th>Upload</th><th>Ping</th><th>Signál</th><th>Testy</th></tr>" +
                "</thead><tbody></tbody></table></div>");
            loadProviderStats({
                year: month.getFullYear(),
                month: month.getMonth() + 1,
                operatorsTable: "#month" + i + "-table",
                devicesTable: null,
                sortBy: [0, "asc"]
            });
            i += 1;
        });
    }

    var loadMonth = function() {
        showLoaderOverlay();
        var numOfLoaded = $(".stats-wrapper .dataTable").length;
        var month = new Date(validMonths[numOfLoaded] + "-01");
        var tableName = "month" + (numOfLoaded + 1) + "-table";
        $(".stats-wrapper").append("<h2 class='mono'>" + monthNames[getPageLanguage()][month.getMonth()] + " " + month.getFullYear() + "</h2>");
        $(".stats-wrapper").append("<div class='table-wrapper'><table id='" + tableName + "'><thead><tr><th>Název</th>" +
            "<th>Stahování</th><th>Upload</th><th>Ping</th><th>Signál</th><th>Testy</th></tr>" +
            "</thead><tbody></tbody></table></div>");

        loadProviderStats({
            year: month.getFullYear(),
            month: month.getMonth() + 1,
            operatorsTable: "#" + tableName,
            devicesTable: null,
            sortBy: [0, "asc"]
        });
    }

    var reloadStats = function() {
        var numOfLoaded = $(".stats-wrapper .dataTable").length;
        $(".stats-wrapper").empty();
        loadMultipleMonths(numOfLoaded);
    }

    getMonthList(true);

    var validMonths = [];

    var waitForMonths = setInterval(function() {
        if ($("#month-select option").length > 0) {
            clearInterval(waitForMonths);
            $("#month-select option").each(function() {
                validMonths.push($(this).attr("value"));
            });
            validMonths.reverse();
            if ($("#month-select option").length > 2) {
                loadMultipleMonths(3);
            } else {
                loadMultipleMonths(1);
            }
        }
    }, 250);

    $("#btn-load-older").click(loadMonth);
    $(".stats-filter select").change(reloadStats);
});
