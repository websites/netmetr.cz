function loadStats(uuid) {
    $.ajax({
        "url": controlUrl + "history",
        "type": "post",
        "dataType": "json",
        "contentType": "application/json",
        "data": JSON.stringify({
            "timezone":   "Europe/Prague",
            "language": getPageLanguage(),
            "uuid": uuid
        }),
        success: function(data) {
            data.history.forEach(function(entry) {
              if (!entry.ping) {
                entry["ping"] = null;
              }
            });

            if (data.history && data.history.length > 0) {
                $(".empty-table-notice").remove();
                statsTable = $("#stats").dataTable({
                    destroy: true,
                    data: data.history,
                    deferRender: true,
                    initComplete: function() {
                        hideLoaderOverlay();
                    },
                    language: dataTablesLang(),
                    columns: [{
                        data: "time_string",
                        render: function(data, type, row) {
                            	   return "<span class=hidden>" + row.time +
                                          "</span><a href='detail.html?" + row.test_uuid + "'>" + row.time_string + "</a>";
                                }
                    }, {
                        data: "model"
                    }, {
                        data: "speed_download",
                        sClass:   "download",
                        render: function(data, type, row) {
                                    return "<span class=classification data-classification=" +
                                    row.speed_download_classification + "></span> " + data;
                                }
                    }, {
                        data: "speed_upload",
                        sClass:   "upload",
                        render: function(data, type, row) {
                                    return "<span class=classification data-classification=" +
                                    row.speed_upload_classification + "></span> " + data;
                                }
                    }, {
                        data: "ping",
                        sClass: "ping",
                        render: function(data, type, row) {
                            var classification = "<span class=classification data-classification=" +
                            (data ? row.ping_classification : 0) + "></span> "
                            if (data) {
                                return classification + "<span class=hidden>" + data.replace(",", ".") + "</span>" + data;
                            } else {
                                return classification + "N/A";
                            }
                        },
                        createdCell: function(td, cellData) {
                            if (cellData == null) {
                                td.classList.add("na");
                            }
                        }
                    }, ],
                order: [
                    [0, "desc"]
                ]
            });
            var getCsvLink = document.querySelector("a#get-csv");
            getCsvLink.href = statisticUrl + "export/client/" + uuid + "/syncgroup/NetMetr-mydata.zip";
            getCsvLink.classList.remove("hidden");
    } else {
        $("#stats tbody").append("<tr><td colspan=5>" + data.error[0] +
            "</td></tr>");
        hideLoaderOverlay();
    }
        },
        error: function() {
            $("#stats tbody").append(
                "<tr><td colspan=5>Nepodařilo se spojit se serverem.</td></tr>");
            hideLoaderOverlay();
        }
    });
}
