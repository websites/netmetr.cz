$(document).ready(function() {
  showLoaderOverlay();

  createCookieIfNotPresent(function() {
    loadStats(Cookies.get(window.cookieName));
  });

  $('input#sync-code').on('paste', function(e) {
    window.setTimeout(function() {
      e.target.value = e.target.value.trim();
    }, 0);
  });

  $('form#sync-code').submit(function(e) {
    e.preventDefault();
    $('p#error-sync').addClass('hidden').text('');
    var syncCode = $('input#sync-code').val().trim();
    var uuid = Cookies.get(window.cookieName);
    if (syncCode.match(/[a-z0-9]{12}/)) {
      $.ajax({
        url: controlUrl + 'sync',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify({
          language: document.documentElement.lang,
          sync_code: syncCode,
          uuid: uuid
        }),
        success: function(data) {
          if (data.sync[0].success === true) {
            console.log('ERROR1');
            $('p#error-sync').addClass('hidden');
            $('p#info-sync')
              .text(data.sync[0].msg_text)
              .removeClass('hidden');
            // refresh:
            showLoaderOverlay();
            loadStats(Cookies.get(window.cookieName));
          } else {
            $('p#error-sync')
              .text(data.sync[0].msg_text)
              .removeClass('hidden');
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          $('p#error-sync')
            .text(thrownError)
            .removeClass('hidden');
        }
      });
    } else {
      $('p#error-sync')
          .text(document.querySelector(".sync-error-format-text").innerText)
          .removeClass('hidden');
    }
  });

  $('#gen-sync-code').click(function(e) {
    e.preventDefault();
    $.ajax({
      url: controlUrl + 'sync',
      type: 'post',
      dataType: 'json',
      contentType: 'application/json',
      data: JSON.stringify({
        language: 'cs',
        uuid: Cookies.get(window.cookieName)
      }),
      success: function(data) {
        $('.your-sync').removeClass('hidden');
        if (data.sync[0]['sync_code']) {
          $('.generated-sync-code').text(data.sync[0]['sync_code']);
        } else {
          $('.generated-sync-code')
            .text(data.sync[0].msg_text)
            .removeClass('hidden');
        }
      },
      error: function(xhr, ajaxOptions, thrownError) {
        $('.your-sync').removeClass('hidden');
        $('.generated-sync-code')
          .text(thrownError)
          .removeClass('hidden');
      }
    });
  });
});
