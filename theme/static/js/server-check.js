$.ajax({
  url: controlUrl + "version",
  error: function () {
    setTimeout(function () {
      hideLoaderOverlay();
    }, 300);
    var notice = document.createElement("div");
    notice.className = "server-down-notice";
    notice.innerHTML =
      document.documentElement.lang === "cs"
        ? "Z nějakého důvodu se nepodařilo spojit s měřícími servery NetMetru. Prosím zkontrolujte si, že spojení nemůže nic blokovat a případně to zkuste později. V mezidobí můžete pro změření rychlosti použít například&nbsp;<a href='https://speedtest.cesnet.cz/'>test od kolegů z CESNETU</a>."
        : "For some unknown reason we failed to connect to the NetMetr measurement servers. Please check that the connection is not blocked and try it again later. In the meantime, you can use&nbsp;<a href='https://speedtest.cesnet.cz/'>a test from colleagues at CESNET</a> to measure speed.";
    document.querySelector("header.page-header").appendChild(notice);
  },
  timeout: 1000,
});
