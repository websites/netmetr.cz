var insufficientDataWarning = {
    "cs": {
        "sZeroRecords": "Nedostatek dat pro vytvoření statistik.",
    },
    "en": {
        "sZeroRecords": "Insufficient data to generate statistics."
    }
}

function loadResults() {
    var daysAgo = parseInt($(".stats-filter #stats-duration").val());
    var sinceDate = new Date();
    sinceDate.setDate(sinceDate.getDate() - daysAgo);
    var untilDate = new Date();

    var params = "?sort_by=time" +
                "&sort_order=desc" +
                "&language=" + getPageLanguage() +
                "&time[]=>" + sinceDate.getFullYear() + "-" + ("0" + (sinceDate.getMonth() + 1)).slice(-2) +
                            "-" + sinceDate.getDate() + " 00:00:00" +
                "&time[]=<" + untilDate.getFullYear() + "-" + ("0" + (untilDate.getMonth() + 1)).slice(-2) +
                            "-" + untilDate.getDate() + " 23:59:59" +
                "&country=cz";

    var technologyParam = $(".stats-filter #stats-net-type-group").val();
    if(/^(2G|3G|4G|5G|LAN|WLAN)$/.test(technologyParam)){
        params += "&cat_technology=" +  technologyParam;
    }


    $("#results").dataTable({
        ajax: {
            url: statisticUrl + "opentests/search" + params,
            dataSrc: "results"
        },
        deferRender: true,
        initComplete: function() {
            hideLoaderOverlay();
        },
        language: dataTablesLang(),
        columns: [{
                data: "time",
                sClass: "datetime",
                render: function(data, type, row) {
                    var links = "<span class='hidden'>" + data + "</span><a href='detail.html?";
                    links = links + row.open_test_uuid + "' class='test-link'>" + UTCToLocal(data) +
                        "</a>";
                    if (row.long && row.lat) {
                        links = links + " <a  title='"+ i18n_stats.areaMap + "' href='map"
                        + (document.documentElement.lang === "cs" ? "a" : "") + ".html?lon=" +
                        row.long + '&amp;lat=' + row.lat + "' class='map-link'></a>";
                    }
                    return links;
                }
            },

            {
                data: "platform",
                sClass: "platform"
            },

            {
                data: "open_test_uuid",
                visible: false,
                searchable: false
            },

            {
                data: "model",
                sClass: "model"
            },

            {
                data: "open_uuid",
                visible: false,
                searchable: false
            },

            {
                data: "signal_strength",
                sClass: "signal",
                sType:   "numeric_ignore_nan",
                render: function(data) {
                    return data || "N/A";
                },
                createdCell: function(td, cellData) {
                    if (cellData == null) {
                        td.classList.add("na");
                    }
                }
            },

            {
                data: "download_kbit",
                sClass: "download",
                render: function(data) {
                    if (data) {
                        return (data / 1000).toFixed(2).replace(".", ",");
                    } else {
                        return 0;
                    }
                }
            },

            {
                data: "ping_ms",
                sClass: "ping",
                render: function(data) {
                    if (data) {
                        return data.toFixed(1).replace(".", ",");
                    } else {
                        return "N/A";
                    }
                },
                createdCell: function(td, cellData) {
                    if (cellData == null) {
                        td.classList.add("na");
                    }
                }
            },

            {
                data: "long",
                visible: false,
                searchable: false
            },

            {
                data: "upload_kbit",
                sClass: "upload",
                render: function(data) {
                    if (data) {
                        return (data / 1000).toFixed(2).replace(".", ",");
                    } else {
                        return 0;
                    }
                }
            },

            {
                data: "lat",
                visible: false,
                searchable: false
            },

            {
                data: "provider_name",
                sClass: "provider"
            }
        ],
        order: [
            [0, "desc"]
        ]
    });
}

var getAjaxData = function() {
    return {
        "language": getPageLanguage(),
        "country": "cz",
        "duration": parseInt($(".stats-filter #stats-duration").val()),
        "network_type_group": $(".stats-filter #stats-net-type-group").val()
    }
}

function loadProviderStats(options) {
    var ajaxData = getAjaxData();

    var operatorsTable = options.operatorsTable;

    if (options.hasOwnProperty("implausible")) {
        ajaxData.implausible = options.implausible;
    }

    if (options.hasOwnProperty("year") && options.hasOwnProperty("month")) {
        ajaxData.year = options.year;
        ajaxData.month = options.month;
    }

    $.ajax({
        "url": statisticUrl + "statistics",
        "type": "post",
        "dataType": "json",
        "contentType": "application/json",
        "data": JSON.stringify(ajaxData),
        success: function(data) {
            var paginateProviders = false;
            if (data.providers.length > 10) {
                paginateProviders = true;
            }
            $(operatorsTable).dataTable({
                data: data.providers,
                destroy: true,
                deferRender: true,
                paging: paginateProviders,
                searching: paginateProviders,
                info: false,
                language: dataTablesLang(insufficientDataWarning),
                initComplete: function() {
                    hideLoaderOverlay();
                    if($(".stats-filter #stats-net-type-group").val() === "4G" || $(".stats-filter #stats-net-type-group").val() === "5G") {
                        $(".signal.minichart").remove();
                    }
                },
                columns: [{
                        data: "shortname"
                    },

                    {
                        data: "quantile_down",
                        sClass: "download minichart",
                        render: function(data, type, row) {
                            if (data) {
                                var chart = "<div class='chart'>" + "<div class='red'" +
                                    "style='width: " + row.down_red * 100 + "%;' " + "title='" + i18n_stats.dload_red + ": " +
                                    (row.down_red * 100).toFixed(1) + " %'>" + "</div><div class='yellow' style='width: " +
                                    row.down_yellow * 100 + "%;' " + "title='" + i18n_stats.dload_yellow + ": " + (row.down_yellow * 100).toFixed(1) +
                                    " %'></div>" + "<div class='green' style='width: " + row.down_green * 100 + "%;' " +
                                    "title='" + i18n_stats.dload_green + ": " + (row.down_green * 100).toFixed(1) + " %'></div></div>";
                                return (data / 1000).toFixed(2).replace(".", ",") + chart;
                            } else {
                                return null;
                            }
                        }
                    },

                    {
                        data: "quantile_up",
                        sClass: "upload minichart",
                        render: function(data, type, row) {
                            if (data) {
                                var chart = "<div class='chart'>" + "<div class='red'" +
                                    "style='width: " + row.up_red * 100 + "%;' " + "title='" + i18n_stats.upload_red + ": " +
                                    (row.up_red * 100).toFixed(1) + " %'>" + "</div><div class='yellow' style='width: " +
                                    row.up_yellow * 100 + "%;' " + "title='" + i18n_stats.upload_yellow + ": " + (row.up_yellow * 100).toFixed(1) +
                                    " %'></div>" + "<div class='green' style='width: " + row.up_green * 100 + "%;' " +
                                    "title='" + i18n_stats.upload_green + ": " + (row.up_green * 100).toFixed(1) + " %'></div></div>";
                                return (data / 1000).toFixed(2).replace(".", ",") + chart;
                            } else {
                                return null;
                            }
                        }
                    },

                    {
                        data: "quantile_ping",
                        render: function(data, type, row) {
                            if (data) {
                                var chart = "<div class='chart'>" + "<div class='red'" +
                                    "style='width: " + row.ping_red * 100 + "%;' " + "title='" + i18n_stats.ping_red + ": " +
                                    (row.ping_red * 100).toFixed(1) + " %'>" + "</div><div class='yellow' style='width: " +
                                    row.ping_yellow * 100 + "%;' " + "title='" + i18n_stats.ping_yellow + ": " + (row.ping_yellow *
                                        100).toFixed(1) + " %'></div>" + "<div class='green' style='width: " + row.ping_green *
                                    100 + "%;' " + "title='" + i18n_stats.ping_green + ": " + (row.ping_green * 100).toFixed(1) + " %'></div></div>";
                                return (data / 1000000).toFixed(1).replace(".", ",") + chart;
                            } else {
                                return null;
                            }
                        },
                        sClass: "ping minichart"
                    },

                    {
                        data: "quantile_signal",
                        render: function(data, type, row) {
                            if (data) {
                                var chart = "<div class='chart'>" + "<div class='red'" +
                                    "style='width: " + row.signal_red * 100 + "%;' " + "title=' " +
                                    (row.signal_red * 100).toFixed(1) + " %'>" + "</div><div class='yellow' style='width: " +
                                    row.signal_yellow * 100 + "%;' " + "title='~ " + (row.signal_yellow * 100).toFixed(1) +
                                    " %'></div>" + "<div class='green' style='width: " + row.signal_green * 100 + "%;' " +
                                    "title='~ " + (row.signal_green * 100).toFixed(1) + " %'></div></div>";
                                return data + chart;
                            } else {
                                return null;
                            }
                        },
                        sClass: "signal minichart"
                    },

                    {
                        data: "count"
                    }
                ],
                order: [
                    options.sortBy
                ]
            });
        },
        error: function() {
            console.error(i18n_general.connectionErr);
            hideLoaderOverlay();
        }
    });
}

function loadDeviceStats(options) {
    var ajaxData = getAjaxData();

    var devicesTable = options.devicesTable;

    if (options.hasOwnProperty("implausible")) {
        ajaxData.implausible = options.implausible;
    }

    if (options.hasOwnProperty("year") && options.hasOwnProperty("month")) {
        ajaxData.year = options.year;
        ajaxData.month = options.month;
    }

    $.ajax({
        "url": statisticUrl + "devices",
        "type": "post",
        "dataType": "json",
        "contentType": "application/json",
        "data": JSON.stringify(ajaxData),
        success: function(data) {
            var paginateDevices = false;
            if (data.devices.length > 10) {
                paginateDevices = true;
            }
            $(devicesTable).dataTable({
                data: data.devices,
                paging: paginateDevices,
                searching: paginateDevices,
                destroy: true,
                deferRender: true,
                language: dataTablesLang(insufficientDataWarning),
                columns: [{
                        data: "model"
                    },

                    {
                        data: "quantile_down",
                        sClass: "download",
                        render: function(data) {
                            return (data / 1000).toFixed(2).replace(".", ",");
                        }
                    },

                    {
                        data: "quantile_up",
                        sClass: "upload",
                        render: function(data) {
                            return (data / 1000).toFixed(2).replace(".", ",");
                        }
                    },

                    {
                        data: "quantile_ping",
                        render: function(data) {
                            return (data / 1000000).toFixed(1).replace(".", ",");
                        },
                        sClass: "ping"
                    },

                    {
                        data: "count"
                    }
                ],
                order: [
                    [4, "desc"]
                ]
            });
        }
    });
}
