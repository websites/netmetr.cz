$(document).ready(function() {
    var statsOptions = {
        operatorsTable: "#operators",
        devicesTable: "#devices",
        sortBy: [0, "asc"]
    }

    var initTables = function() {
        $(".table-wrapper.operators").html("<table id='operators'><thead><tr><th>"+i18n_stats.provider+"</th><th>"+i18n_stats.download+"</th>" +
            "<th>"+i18n_stats.upload+"</th><th>"+i18n_stats.ping+"</th><th>"+i18n_stats.signal+"</th><th>"+i18n_stats.test_count+"</th></tr></thead><tbody></tbody></table>");

        $(".table-wrapper.devices").html("<table id='devices'><thead><tr><th>"+i18n_stats.device+"</th><th>"+i18n_stats.download+"</th>" +
                "<th>"+i18n_stats.upload+"</th><th>"+i18n_stats.ping+"</th><th>"+i18n_stats.test_count+"</th></tr></thead><tbody></tbody></table>");

        $(".table-wrapper.results").html("<table id='results'><thead><tr><th>"+i18n_stats.test_time+"</th><th>OS</th><th>open_test_uuid</th>" +
                "<th>"+i18n_stats.device+"</th><th>open_uuid</th><th>"+i18n_stats.signal+"</th><th>"+i18n_stats.download+"</th><th>"+i18n_stats.ping+"</th><th>long</th>" +
                "<th>"+i18n_stats.upload+"</th><th>lat</th><th>"+i18n_stats.provider+"</th></tr></thead><tbody></tbody></table>");
    }

    var loadStats = function() {
        showLoaderOverlay();
        initTables();
        loadProviderStats(statsOptions);
        loadDeviceStats(statsOptions);
        loadResults();
    }

    loadStats();

    $(".stats-filter select").change(loadStats);
});

$(window).resize(function() {
    $("table").css("width", "100%");
});
