netmetr = {
    i18n: {}
};

baseUrl = "https://control.netmetr.cz/";
baseUrl4 = "https://control-4.netmetr.cz/";
baseUrl6 = "https://control-6.netmetr.cz/";
controlUrl = baseUrl + "RMBTControlServer/";
controlUrl4 = baseUrl4 + "RMBTControlServer/";
controlUrl6 = baseUrl6 + "RMBTControlServer/";
statisticUrl = baseUrl + "RMBTStatisticServer/";
mapServer = baseUrl + "RMBTMapServer/tiles/";
