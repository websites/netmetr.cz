function setBarPercentage(barSelector, percents) {
  var bar = document.querySelector(barSelector);
  if (!bar) {
    console.error("Element not found: " + barSelector + ".");
  } else {
    bar.style.strokeDasharray =
      bar.getTotalLength() * (percents / 100) + ",9999";
  }
}

function setTextPercentage(textSelector, percents) {
  var text = $(textSelector);
  var percentage = percents + " %";
  if (!text) {
    console.error("Element not found: " + textSelector + ".");
  } else if (text.text() !== percentage) {
    text.text(percentage);
  }
}

function setTextStatus(textSelector, status) {
  var text = $(textSelector);
  if (!text) {
    console.error("Element not found: " + textSelector + ".");
  } else if (text.text() !== status) {
    text.text(status);
  }
}

function formatBytes(bytes) {
  if (bytes == 0) return "0 B";
  var k = 1000,
    sizes = ["b/s", "Kb/s", "Mb/s", "Gb/s"],
    i = Math.floor(Math.log(bytes) / Math.log(k));
  return parseFloat((bytes / Math.pow(k, i)).toFixed(1)) + " " + sizes[i];
}

function setSpeedStatus(textSelector, speed) {
  var text = $(textSelector);
  var speedWithUnits = formatBytes(speed);
  if (!text) {
    console.error("Element not found: " + textSelector + ".");
  } else {
    if (speed !== null) {
      if (text.text() !== speedWithUnits) {
        text.text(speedWithUnits);
      }
    } else {
      text.text("");
    }
  }
}

function getBackendUrl() {
  var opts = window.location.search.substring(1).split("&");
  if (opts.indexOf("ipv4") >= 0) {
    return baseUrl4;
  } else if (opts.indexOf("ipv6") >= 0) {
    return baseUrl6;
  } else {
    return baseUrl;
  }
}

function runTest() {
  var backendUrl = getBackendUrl();
  var config = new RMBTTestConfig(
    getPageLanguage(),
    backendUrl,
    "RMBTControlServer"
  );
  var ctrl = new RMBTControlServerCommunication(config);
  config.uuid = Cookies.get().netmetr_uuid;
  var websocketTest = new RMBTTest(config, ctrl);
  websocketTest.startTest();

  if (document.querySelector(".gauge")) {
    window.setInterval(function() {
      updateGauge(websocketTest.getIntermediateResult());
    }, 200);
  }

  $(".start-test").attr("disabled", true);
  $(".start-test").css("display", "none");
}

function logPercent(value) {
  if (value <= 0) {
    return 0;
  }
  var percents = value / (1.25 / 100 / value); // 1.25 is *BitPerSecLog for max. value (1 Gbps)
  return Math.min(100, percents);
}

statusText = {
  init: {
    cs: "příprava testu",
    en: "preparing speedtest"
  },
  init_down: {
    cs: "příprava downloadu",
    en: "preparing download"
  },
  ping: {
    cs: "ping",
    en: "ping"
  },
  down: {
    cs: "download",
    en: "download"
  },
  init_up: {
    cs: "příprava uploadu",
    en: "preparing upload"
  },
  up: {
    cs: "upload",
    en: "upload"
  },
  end: {
    cs: "hotovo",
    en: "finished"
  }
};

function updateGauge(r) {
  setTextPercentage("#percents", Math.ceil(r.progress * 100));
  switch (r.status) {
    case "INIT":
      setBarPercentage("#init", r.progress * 100);
      setTextStatus("#status", statusText["init"][getPageLanguage()]);
      break;
    case "INIT_DOWN":
      setBarPercentage("#init", 100);
      setTextStatus("#status", statusText["init_down"][getPageLanguage()]);
      break;
    case "PING":
      setBarPercentage("#ping", r.progress * 100);
      setTextStatus("#status", statusText["ping"][getPageLanguage()]);
      break;
    case "DOWN":
      setBarPercentage("#download", r.progress * 100);
      setTextStatus("#status", statusText["down"][getPageLanguage()]);
      setSpeedStatus("#speedtext", r.downBitPerSec);
      setBarPercentage("#speed", logPercent(r.downBitPerSecLog));
      $(".colors-down").removeClass("transparent");
      break;
    case "INIT_UP":
      setBarPercentage("#download", 100);
      setTextStatus("#status", statusText["init_up"][getPageLanguage()]);
      setSpeedStatus("#speedtext", null);
      setBarPercentage("#speed", 0);
      $(".colors-down").addClass("transparent");
      break;
    case "UP":
      setBarPercentage("#upload", r.progress * 100);
      setTextStatus("#status", statusText["up"][getPageLanguage()]);
      setSpeedStatus("#speedtext", r.upBitPerSec);
      setBarPercentage("#speed", logPercent(r.upBitPerSecLog));
      $(".colors-up").removeClass("transparent");
      break;
    case "END":
      setTextStatus("#status", statusText["end"][getPageLanguage()]);
      setSpeedStatus("#speedtext", null);
      setBarPercentage("#speed", 0);
      $(".colors-up").addClass("transparent");
      break;
  }
}
