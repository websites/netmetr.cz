/* getParam polyfill */
if (typeof window.getParam === "undefined") {
  window.getParam = function(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
      var pair = vars[i].split("=");
      if (pair[0] == variable) {
        return pair[1];
      }
    }
    return false;
  };
}

$(function() {
  createCookieIfNotPresent(
    function() {
      $(".start-test").attr("disabled", false);
      $(".start-test").click(runTest);

      var opts = window.location.search.substring(1).split("&");

      if (opts.indexOf("start") >= 0) {
        runTest();
      }
    },
    function() {
      $(".start-test").attr("disabled", true);
      console.error(i18n_general.connectionErr);
    }
  );
});
